package com.lhw.dataloader;

import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_DATE_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_REFRESH_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_DATE_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_REFRESH_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_DATE_COLUMN_1;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_DATE_COLUMN_2;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_REFRESH_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_TABLE_NAME;
import static com.lhw.dataloader.common.JobName.LHW_DAILY_JOB;
import static com.lhw.dataloader.common.JobName.RELOAD_BOOKING_RAW_FACT_JOB;
import static com.lhw.dataloader.common.JobName.RELOAD_LC_MEMBER_JOB;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Future;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.JobName;
import com.lhw.dataloader.job.DimJob;
import com.lhw.dataloader.job.Job;
import com.lhw.dataloader.job.JobManager;
import com.lhw.dataloader.job.JobStats;
import com.lhw.dataloader.job.LhwDailyJob;
import com.lhw.dataloader.job.RefreshBookingMstrDimJob;
import com.lhw.dataloader.job.RefreshBookingRawFactJob;
import com.lhw.dataloader.job.RefreshBookingStatusDimJob;
import com.lhw.dataloader.job.RefreshLcEnrollmentsJob;
import com.lhw.dataloader.job.ReloadBookingRawFactJob;
import com.lhw.dataloader.job.ReloadLcMemberJob;
import com.lhw.dataloader.util.ConnUtils;

public final class DataLoader {
	private static final Logger logger = LoggerFactory.getLogger(DataLoader.class.getName());
	
	private static final String DEFAULT_HELP_MSG = "java -jar VeroClient.jar";
	
	private DataLoader() {
	}

	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			JobManager.I.shutdown();
			ConnUtils.I.shutdown();
		}));
		
		Options options = createCommandLineOptions();
		HelpFormatter formatter = new HelpFormatter();
		
		if (args == null || args.length < 1) {
			formatter.printHelp(DEFAULT_HELP_MSG, options, true);
			System.exit(0);
		}
		
		CommandLineParser commandLineParser = new DefaultParser();		
		
		try {			
			CommandLine commandLine = commandLineParser.parse(options, args);
			
			String command = commandLine.getOptionValue("j");			
			if (command == null || command.trim().equals("")) {
				System.out.println("Job option is required.");
				formatter.printHelp(DEFAULT_HELP_MSG, options, true);
				System.exit(0);
			}
			
			JobName jobName = JobName.toJobName(commandLine.getOptionValue("j"));
			
			if (jobName == null) {
				System.out.println("Invalid job name.");
				formatter.printHelp(DEFAULT_HELP_MSG, options, true);
				System.exit(0);
			}
			
			Date startTime = null; 
			if (commandLine.hasOption("st")) {
				try {
    				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    				startTime = dateFormat.parse(commandLine.getOptionValue("st"));
				}
				catch (Exception e) {
					System.out.println("Invalid start time");
					formatter.printHelp(DEFAULT_HELP_MSG, options, true);
					System.exit(0);
				}
			}
			
			long interval = -1;
			if (commandLine.hasOption("i")) {
				try {
					interval = Long.parseLong(commandLine.getOptionValue("i"));
				}
				catch (Exception e) {
					System.out.println("Invalid interval");
					formatter.printHelp(DEFAULT_HELP_MSG, options, true);
					System.exit(0);
				}
			}
			
			File baseDir = null;
			if (commandLine.hasOption("bd")) {
				try {
					baseDir = new File(commandLine.getOptionValue("bd"));
					if (!baseDir.exists())
						throw new Exception("Invalid base directory");
				}
				catch (Exception e) {
					System.out.println("Invalid base directory");
					formatter.printHelp(DEFAULT_HELP_MSG, options, true);
					System.exit(0);
				}
			}
			
			File dataFile = null;
			if (commandLine.hasOption("dn")) {
				try {
					dataFile = new File(commandLine.getOptionValue("dn"));
					if (!dataFile.exists())
						throw new Exception("Invalid data file name");
				}
				catch (Exception e) {
					System.out.println("Invalid data file name");
					formatter.printHelp(DEFAULT_HELP_MSG, options, true);
					System.exit(0);
				}
			}
		
			if (jobName == LHW_DAILY_JOB || jobName == RELOAD_LC_MEMBER_JOB
					|| jobName == RELOAD_BOOKING_RAW_FACT_JOB) {
				if (baseDir == null && dataFile == null) {
					System.out.println("Base directory or data file name is required for this job");
					formatter.printHelp(DEFAULT_HELP_MSG, options, true);
					System.exit(0);
				}
			}
			
			Job<JobStats> job = null;
		
			switch (jobName) {
			case LHW_DAILY_JOB:
				job = new LhwDailyJob(baseDir);
				break;
			case DIM_JOB:
				job = new DimJob();
				break;
			case REFRESH_BOOKING_MSTR_DIM_JOB:
				job = new RefreshBookingMstrDimJob(BOOKING_MSTR_DIM_TABLE_NAME, BOOKING_MSTR_DIM_KEY_COLUMN, 
						BOOKING_MSTR_DIM_DATE_COLUMN, BOOKING_MSTR_DIM_REFRESH_REPORT_ID);
				break;
			case REFRESH_LC_ENROLLMENTS_JOB:
				job = new RefreshLcEnrollmentsJob(LC_ENROLLMENTS_TABLE_NAME, 
						LC_ENROLLMENTS_KEY_COLUMN, LC_ENROLLMENTS_DATE_COLUMN_1, LC_ENROLLMENTS_DATE_COLUMN_2, 
						LC_ENROLLMENTS_REFRESH_REPORT_ID);
				break;
			case RELOAD_LC_MEMBER_JOB:
				job = baseDir == null ? new ReloadLcMemberJob(dataFile.getAbsolutePath()) : new ReloadLcMemberJob(baseDir);
				break;
			case RELOAD_BOOKING_RAW_FACT_JOB:
				job = baseDir == null ? new ReloadBookingRawFactJob(dataFile.getAbsolutePath()) : new ReloadBookingRawFactJob(baseDir);
				break;
			case REFRESH_BOOKING_RAW_FACT_JOB:
				job = baseDir == null ? new RefreshBookingRawFactJob(dataFile.getAbsolutePath()) : new RefreshBookingRawFactJob(baseDir);
				break;
			case REFRESH_BOOKING_STATUS_DIM_JOB:
				job = new RefreshBookingStatusDimJob(BOOKING_STATUS_DIM_TABLE_NAME, BOOKING_STATUS_DIM_KEY_COLUMN, 
						BOOKING_STATUS_DIM_DATE_COLUMN, BOOKING_STATUS_DIM_REFRESH_REPORT_ID);
				break;
			default:
				System.out.println("Invalid job name.");
				formatter.printHelp(DEFAULT_HELP_MSG, options, true);
				System.exit(0);
			}
			
		    Future<?> future = null;
			if (startTime == null) {
				// Run it once
				future = JobManager.I.execute(job);
			}
			else if (interval == -1) {
				// Run it once at a specific time
				future = JobManager.I.schedule(job, startTime);
			}
			else {
				future = JobManager.I.schedule(job, startTime, interval);
			}
			
			future.get();
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}		
		
		// Signal system shutdown (Otherwise HikariCP is still running
		System.exit(0);
	}
	
	private static Options createCommandLineOptions() {
		Options options = new Options();
		
		options.addOption("h", "help", false, "Show this help list.");
		options.addOption("j", "job", true, "Job to execute, ex. LhwDailyJob, RefreshFactDataJob, etc.");
		options.addOption("st", "startTime", true, "When to start the job.(yyyy-MM-dd HH:mm:ss, ex. 2016-03-01 14:00:00)");
		options.addOption("i", "interval", true, "How many minutes between each execution.");
		options.addOption("bd", "baseDir", true, "Base directory for all input files");
		options.addOption("dn", "dataFileName", true, "A specific file instead of baseDir");
		
		return options;
	}
}
