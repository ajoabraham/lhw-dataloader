package com.lhw.dataloader.job;

import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_DATE_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_REFRESH_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_DATE_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_REFRESH_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_DATE_COLUMN_1;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_DATE_COLUMN_2;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_REFRESH_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_TABLE_NAME;

import java.io.File;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.job.JobStats.JobState;
import com.lhw.dataloader.util.MailService;

// This job should be scheduled daily to run the following tasks.
// 1. Recreate all dimension tables.
// 2. Refresh booking mstr dim data table.
// 3. Refresh Lc Enrollment data table.
// TH 02/15/2017, the following two items are moved into lodr.io
// 4. Reload Lc Member data table.
// 5. Refresh booking fact raw data table.
public class LhwDailyJob implements Job<JobStats> {
	private static final Logger logger = LoggerFactory.getLogger(LhwDailyJob.class.getName());
	
//	private File baseDir = null;
	
	public LhwDailyJob(File baseDir) {
//		this.baseDir = baseDir;
	}
	
	@Override
	public JobStats call() throws Exception {
		JobStats jobStats = new JobStats();
		jobStats.setName("LHW Daily Data Refresh");
		jobStats.setStartTime(new Date());
		
		logger.info("LHW Daily Data Refresh started on " + jobStats.getStartTime().toString());
		
		DimJob dimJob = new DimJob();
		jobStats.addChildJobStats(dimJob.call());
		
		RefreshBookingMstrDimJob refreshBookingMstrDimJob = new RefreshBookingMstrDimJob(BOOKING_MSTR_DIM_TABLE_NAME, BOOKING_MSTR_DIM_KEY_COLUMN, 
				BOOKING_MSTR_DIM_DATE_COLUMN, BOOKING_MSTR_DIM_REFRESH_REPORT_ID);
		jobStats.addChildJobStats(refreshBookingMstrDimJob.call());
		
		RefreshBookingStatusDimJob refreshBookingStatusDimJob = new RefreshBookingStatusDimJob(BOOKING_STATUS_DIM_TABLE_NAME, BOOKING_STATUS_DIM_KEY_COLUMN, 
				BOOKING_STATUS_DIM_DATE_COLUMN, BOOKING_STATUS_DIM_REFRESH_REPORT_ID);
		jobStats.addChildJobStats(refreshBookingStatusDimJob.call());
		
		RefreshLcEnrollmentsJob refreshLcEnrollmentsJob = new RefreshLcEnrollmentsJob(LC_ENROLLMENTS_TABLE_NAME, 
				LC_ENROLLMENTS_KEY_COLUMN, LC_ENROLLMENTS_DATE_COLUMN_1, LC_ENROLLMENTS_DATE_COLUMN_2, 
				LC_ENROLLMENTS_REFRESH_REPORT_ID);
		jobStats.addChildJobStats(refreshLcEnrollmentsJob.call());
		
		// TH 02/15/2017, Moved into lodr.io
//		ReloadLcMemberJob reloadLcMemberJob = new ReloadLcMemberJob(baseDir);
//		jobStats.addChildJobStats(reloadLcMemberJob.call());
//		
//		RefreshBookingRawFactJob refreshBookingRawFactJob = new RefreshBookingRawFactJob(baseDir);
//		jobStats.addChildJobStats(refreshBookingRawFactJob.call());
		
		jobStats.setEndTime(new Date());
		jobStats.setState(JobState.SUCCEEDED);
		
		String msg = null;
		String subject = null;
		if (jobStats.isSucceeded()) {
			msg = "Successfully completed all jobs, started on " + jobStats.getStartTime().toString()
					+ " and ended on " + jobStats.getEndTime().toString();
			subject = "Succeeded";
		}
		else {
			msg = "Failed to completed all jobs, please check below for more details. Job started on "
					+ jobStats.getStartTime().toString() + " and ended on " + jobStats.getEndTime().toString();
			subject = "Failed";
		}
		
		jobStats.addMessage(msg);		
		logger.info(msg);
		MailService.I.sendMail("LHW Daily Refresh Status Report (" + subject + ")", jobStats.buildEmailBody());

		return jobStats;
	}

}
