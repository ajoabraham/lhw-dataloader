package com.lhw.dataloader.job;
import static com.lhw.dataloader.common.JobConstants.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.job.JobStats.JobState;
import com.lhw.dataloader.job.processor.LoadDataProcessor;
import com.lhw.dataloader.job.processor.LoadDataProcessor.LoadDataProcessorBuilder;
import com.lhw.dataloader.util.ConnUtils;

public class RefreshBookingStatusDimJob implements Job<JobStats> {
	private static final Logger logger = LoggerFactory.getLogger(RefreshBookingStatusDimJob.class.getName());
	
	private String tableName = null;
	private String keyColumnName = null;
	private String dateColumnName = null;
	private String reportId = null;
	
	public RefreshBookingStatusDimJob(String tableName, String keyColumnName, String dateColumnName, String reportId) {
		this.tableName = tableName;
		this.keyColumnName = keyColumnName;
		this.dateColumnName = dateColumnName;
		this.reportId = reportId;
	}

	@Override
	public JobStats call() throws Exception {
		JobStats jobStats = new JobStats();
		jobStats.setName("Refresh " + BOOKING_STATUS_DIM_TABLE_NAME);
		jobStats.setStartTime(new Date());
		long startTime = System.currentTimeMillis();
		
		try {
    		// End date is yesterday
    		Date endDate = DateUtils.addDays(new Date(), -1);
    		// Last updated date minus refresh interval
    		Date startDate = DateUtils.addDays(getLastDate(tableName, dateColumnName), 1 - AppProps.I.getFactRefreshInterval());
    		LoadDataProcessor proc = LoadDataProcessorBuilder.create().setSessionStateUrl(AppProps.I.getSessionStateUrl())
    				.setDataUrlTemplate(AppProps.I.getFactDataUrlTemplate()) 
    				.setStartDate(startDate)
    				.setEndDate(endDate)
    				.setLoadInterval(AppProps.I.getDataLoadInterval())
    				.setTableName(tableName)
    				.setKeyColumnName(keyColumnName)
    				.setLoadMode(LoadMode.UPSERT)
    				.setIgnoreLastColumn(true)
    				.setReportIds(reportId).build();
    		proc.process();
    		
    		String msg = "Successfully refreshed " + BOOKING_STATUS_DIM_TABLE_NAME + " in "
    				+ (System.currentTimeMillis() - startTime) + "ms";
    		jobStats.addMessage(msg);
    		jobStats.setState(JobState.SUCCEEDED);
    		
    		logger.info(msg);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			String msg = "Failed to refresh " + BOOKING_STATUS_DIM_TABLE_NAME + " - "
					+ e.getMessage();
			jobStats.addMessage(msg);
			jobStats.setState(JobState.FAILED);
		}
		
		jobStats.setEndTime(new Date());
		return jobStats;
	}

	private static final String LAST_DATE_QUERY = "select max(%s) from %s";
	private Date getLastDate(String tableName, String dateColumnName) throws SQLException {
		Date lastDate = null;
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			
			ResultSet resultSet = statement.executeQuery(String.format(LAST_DATE_QUERY, dateColumnName, tableName));
			
			if (resultSet.next()) {
				lastDate = resultSet.getDate(1);
			}
		}
		
		return lastDate;
	}
}
