package com.lhw.dataloader.job;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.job.processor.LoadDataProcessor;
import com.lhw.dataloader.job.processor.LoadDataProcessor.LoadDataProcessorBuilder;
import com.lhw.dataloader.util.ConnUtils;

public class RefreshFactDataJob implements Job<Void> {
	private String tableName = null;
	private String keyColumnName = null;
	private String dateColumnName = null;
	private String reportId = null;
	
	public RefreshFactDataJob(String tableName, String keyColumnName, String dateColumnName, String reportId) {
		this.tableName = tableName;
		this.keyColumnName = keyColumnName;
		this.dateColumnName = dateColumnName;
		this.reportId = reportId;
	}

	@Override
	public Void call() throws Exception {
		// End date is yesterday
		Date endDate = DateUtils.addDays(new Date(), -1);
		// Last updated date minus refresh interval
		// Limits the data being processed.
		// If today you only load the day before yesterday to yesterday
		Date startDate = DateUtils.addDays(getLastDate(tableName, dateColumnName), 1 - AppProps.I.getFactRefreshInterval());
		LoadDataProcessor proc = LoadDataProcessorBuilder.create().setSessionStateUrl(AppProps.I.getSessionStateUrl())
				.setDataUrlTemplate(AppProps.I.getFactDataUrlTemplate()) 
				.setStartDate(startDate)
				.setEndDate(endDate)
				.setLoadInterval(AppProps.I.getDataLoadInterval())
				.setTableName(tableName)
				.setKeyColumnName(keyColumnName)
				.setLoadMode(LoadMode.UPSERT)
				.setIgnoreLastColumn(true)
				.setReportIds(reportId).build();
		proc.process();
		return null;
	}

	private static final String LAST_DATE_QUERY = "select max(%s) from %s";

	private Date getLastDate(String tableName, String dateColumnName) throws SQLException {
		Date lastDate = null;
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			
			ResultSet resultSet = statement.executeQuery(String.format(LAST_DATE_QUERY, dateColumnName, tableName));

			if (resultSet.next()) {
				lastDate = resultSet.getDate(1);
			}
		}
		
		return lastDate;
	}
}
