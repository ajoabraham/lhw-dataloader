package com.lhw.dataloader.job;

import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_TABLE_NAME;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.job.JobStats.JobState;
import com.lhw.dataloader.job.processor.LoadDataProcessor;
import com.lhw.dataloader.job.processor.LoadDataProcessor.LoadDataProcessorBuilder;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.ConnUtils;

public class RefreshLcEnrollmentsJob implements Job<JobStats> {
	private static final Logger logger = LoggerFactory.getLogger(RefreshLcEnrollmentsJob.class.getName());
	
	private String tableName = null;
	private String keyColumnName = null;
	private String dateColumnName1 = null;
	private String dateColumnName2 = null;
	private String reportId = null;
	
	public RefreshLcEnrollmentsJob(String tableName, String keyColumnName, String dateColumnName1, String dateColumnName2, String reportId) {
		this.tableName = tableName;
		this.keyColumnName = keyColumnName;
		this.dateColumnName1 = dateColumnName1;
		this.dateColumnName2 = dateColumnName2;
		this.reportId = reportId;
	}

	@Override
	public JobStats call() throws Exception {
		JobStats jobStats = new JobStats();
		jobStats.setName("Refresh " + LC_ENROLLMENTS_TABLE_NAME);
		jobStats.setStartTime(new Date());
		long startTime = System.currentTimeMillis();
		
		try {
    		// End date is yesterday
    		Date endDate = DateUtils.addDays(new Date(), -1);
    		// Last updated date minus refresh interval
    		Date startDate = DateUtils.addDays(getLastDate(tableName, dateColumnName1, dateColumnName2), 1 - AppProps.I.getLcEnrollmentsRefreshInterval());
    		LoadDataProcessor proc = LoadDataProcessorBuilder.create().setSessionStateUrl(AppProps.I.getSessionStateUrl())
    				.setDataUrlTemplate(AppProps.I.getFactDataUrlTemplate()) 
    				.setStartDate(startDate)
    				.setEndDate(endDate)
    				.setLoadInterval(AppProps.I.getDataLoadInterval())
    				.setLoadMode(LoadMode.UPSERT)
    				.setTableName(tableName)
    				.setKeyColumnName(keyColumnName)
    				.setReportIds(reportId).setLcEnrollments(true).build();
    		proc.process();
    		CommonUtils.recreateViews();
    		CommonUtils.grantPermission();
    		
    		String msg = "Successfully refreshed " + LC_ENROLLMENTS_TABLE_NAME + " in "
    				+ (System.currentTimeMillis() - startTime) + "ms";
    		jobStats.addMessage(msg);
    		jobStats.setState(JobState.SUCCEEDED);
    		
    		logger.info(msg);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			String msg = "Failed to refresh " + LC_ENROLLMENTS_TABLE_NAME + " - "
					+ e.getMessage();
			jobStats.addMessage(msg);
			jobStats.setState(JobState.FAILED);
		}
		
		return jobStats;
	}

	private static final String LAST_DATE_QUERY = "select max(case when %s >= %s then %s else %s end) from %s";
	private Date getLastDate(String tableName, String dateColumnName1, String dateColumnName2) throws SQLException {
		Date lastDate = null;
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			
			ResultSet resultSet = statement.executeQuery(String.format(LAST_DATE_QUERY, dateColumnName1,
					dateColumnName2, dateColumnName1, dateColumnName2, tableName));
			
			if (resultSet.next()) {
				lastDate = resultSet.getDate(1);
			}
		}
		
		return lastDate;
	}

}
