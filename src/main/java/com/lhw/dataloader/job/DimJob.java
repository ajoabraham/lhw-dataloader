package com.lhw.dataloader.job;

import static com.lhw.dataloader.common.JobConstants.DIM_REPORT_IDS;
import static com.lhw.dataloader.common.JobConstants.DIM_TABLE_NAMES;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.job.JobStats.JobState;
import com.lhw.dataloader.job.processor.DimDataProcessor;
import com.lhw.dataloader.util.CommonUtils;

/**
 * A DimDataJob is representing a unit of work, which will read data from source, process it
 * and load it up into Redshift. Each job will be executed in a separated thread. This job is
 * only suitable to load small amount of data (such as dimension tables).
 * 
 * @author Tai Hu
 *
 */
public class DimJob implements Job<JobStats> {	
	private static final Logger logger = LoggerFactory.getLogger(DimJob.class.getName());
	
	public DimJob() {
	}

	@Override
	public JobStats call() throws Exception {
		JobStats jobStats = new JobStats();
		jobStats.setName("Reload all dimension tables");
		jobStats.setStartTime(new Date());
		
		for (int i = 0; i < DIM_REPORT_IDS.length; i++) {
			long startTime = System.currentTimeMillis();
			JobStats childJobStats = new JobStats();
			childJobStats.setName("Reload " + DIM_TABLE_NAMES[i] + " table");
			childJobStats.setStartTime(new Date());
			
			try {				
    			DimDataProcessor proc = new DimDataProcessor(AppProps.I.getSessionStateUrl(), 
    					AppProps.I.getDimDataUrlTemplate(), DIM_TABLE_NAMES[i], DIM_REPORT_IDS[i]);
    			proc.process();
    			CommonUtils.grantPermission();
    			String msg = "Successfully reloaded " + DIM_TABLE_NAMES[i] + " in "
    					+ (System.currentTimeMillis() - startTime) + "ms";
    			childJobStats.addMessage(msg);
    			childJobStats.setState(JobState.SUCCEEDED);
    			
    			logger.info(msg);
			}
			catch (Exception e) {
				logger.error(e.getMessage(), e);
				String msg = "Failed to reload " + DIM_TABLE_NAMES[i] + " - "
						+ e.getMessage();
				childJobStats.addMessage(msg);
				childJobStats.setState(JobState.FAILED);
			}
			
			childJobStats.setEndTime(new Date());
			jobStats.addChildJobStats(childJobStats);
		}
		
		jobStats.setEndTime(new Date());
		jobStats.setState(JobState.SUCCEEDED);
		return jobStats;
	}
}
