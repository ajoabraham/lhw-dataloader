package com.lhw.dataloader.job.processor;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.csv.CSVPrinter;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.job.publisher.DataPublisher;
import com.lhw.dataloader.job.publisher.DbReplaceDataPublisher;
import com.lhw.dataloader.job.reader.DataReader;
import com.lhw.dataloader.job.reader.RestDataReader;
import com.lhw.dataloader.model.DataRow;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

/**
 * This is the main work horse of data loading process. A DimDataProcessor is using
 * RestDataReader to read data from one or more data sources based on report ID. Then
 * DimDataProcessor will scan through all data one row at a time and feed them into 
 * TypeAnalyzer (one TypeAnalyzer per column). At the same time, DimDataProcessor 
 * write that row into a local CSV file. If multiple report IDs provided, all data
 * read from multiple requests will be combined into one local CSV file. Once this
 * process is done, DimDataProcessor will generate DDL based on information in all
 * TypeAnalyzers and upload all data from local CSV file into Redshift
 * 
 * @author Tai Hu
 *
 */
public class DimDataProcessor implements DataProcessor {
//	private static final Logger logger = LoggerFactory.getLogger(DimDataProcessor.class.getName());

	private String[] reportIds = null;
	private String sessionStateUrl = null;
	private String dataUrlTemplate = null;
	private String tableName = null;

	public DimDataProcessor(String sessionStateUrl, String dataUrlTemplate, String tableName, String... reportIds) {
		this.reportIds = reportIds;
		this.sessionStateUrl = sessionStateUrl;
		this.dataUrlTemplate = dataUrlTemplate.replace(CommonConstants.PARAM_DELIMITER, 
				CommonUtils.encodeUrl(AppProps.I.getCsvDelimiter()));
		this.tableName = tableName;
	}

	@Override
	public void process() throws ProcessorException {
		try {
			File tempFile = new File(CommonUtils.generateFileName(tableName));
			Map<String, TypeAnalyzer> typeAnalyzers = new LinkedHashMap<>();
			HeaderRow headerRow = pullAndAnalyzeData(tempFile, typeAnalyzers);

			DataPublisher dataPublisher = new DbReplaceDataPublisher(tableName, headerRow, typeAnalyzers, tempFile);
			dataPublisher.publish();
		}
		catch (Exception e) {
			throw new ProcessorException(e.getMessage(), e);
		}
	}

	HeaderRow pullAndAnalyzeData(File tempFile,
			Map<String, TypeAnalyzer> typeAnalyzers) throws Exception {
		// Read in all the data and stored into temp file locally
		HeaderRow headerRow = null;
		try (CSVPrinter out = CommonUtils.createCSVPrinter(tempFile)) {
			for (String reportId : reportIds) {
				String dataUrl = dataUrlTemplate.replace(CommonConstants.PARAM_REPORT_ID, reportId);
				try (DataReader dataReader = new RestDataReader(sessionStateUrl, dataUrl)) {
					HeaderRow hr = dataReader.getHeaderRow();					
					if (headerRow == null) {
						headerRow = hr;
						out.printRecord(headerRow.getHeaderNames());
					}
					else if (headerRow.size() != hr.size()) {
						throw new ProcessorException("Header column sizes are not matched (expected: " + headerRow.size() + ", actual: " + hr.size()
								+ ") for Report ID: " + reportId);
					}

					DataRow dataRow = null;

					while ((dataRow = dataReader.readDataRow()) != null) {
						for (int i = 0; i < headerRow.getHeaderNames().size(); i++) {
							String headerName = headerRow.getHeaderNames().get(i);
							TypeAnalyzer typeAnalyzer = typeAnalyzers.get(
									headerName);
							if (typeAnalyzer == null) {
								typeAnalyzer = new TypeAnalyzer(CommonUtils.normalizeColumnName(headerName), i);
								typeAnalyzers.put(headerName, typeAnalyzer);
							}

							String value = dataRow.getValue(headerName);
							typeAnalyzer.analyze(value);
						}

						out.printRecord(dataRow.getValues());
					}

				}
			}
		}

		return headerRow;
	}
}
