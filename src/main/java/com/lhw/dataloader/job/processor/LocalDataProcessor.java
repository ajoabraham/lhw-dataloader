package com.lhw.dataloader.job.processor;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.csv.CSVPrinter;

import com.lhw.dataloader.job.publisher.DataPublisher;
import com.lhw.dataloader.job.publisher.DbReplaceDataPublisher;
import com.lhw.dataloader.job.reader.CsvFileDataReader;
import com.lhw.dataloader.model.DataRow;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

/**
 * LocalDataProcessor is similar to DimDataProcessor. The difference is
 * LocalDataProcessor will read data from a list of local CSV files instead of
 * remote Microstrategy server. LocalDataProcessor will analyze all data and store
 * them into one single local CSV file and upload it into Redshift. NOTES: All input CSV
 * files should have same columns (intend for same table). Redshift Insert performance is relatively
 * slow (about 2.5 seconds per 3000 rows). For really large dataset, use RedshiftDataProcessor
 * to produce Redshift COPY command ready CSV files and use COPY command to insert data into Redshift.
 * 
 * @author Tai Hu
 *
 */
public class LocalDataProcessor implements DataProcessor {
//	private static final Logger logger = LoggerFactory.getLogger(LocalDataProcessor.class.getName());

	protected String tableName = null;
	protected String[] dataFileNames = null;
	
	private String csvDelimiter = null;
	private String csvDelimiterRegex = null;

	public LocalDataProcessor(String tableName, String... dataFileNames) {
		this.tableName = tableName;
		this.dataFileNames = dataFileNames;
	}

	@Override
	public void process() throws ProcessorException {
		try {
			File tempFile = new File(CommonUtils.generateFileName(tableName));
			Map<String, TypeAnalyzer> typeAnalyzers = new LinkedHashMap<>();
			HeaderRow headerRow = pullAndAnalyzeData(tempFile, typeAnalyzers);

			DataPublisher dataPublisher = new DbReplaceDataPublisher(tableName, headerRow, typeAnalyzers, tempFile);
			dataPublisher.publish();
		}
		catch (Exception e) {
			throw new ProcessorException(e.getMessage(), e);
		}
	}

	HeaderRow pullAndAnalyzeData(File tempFile,
			Map<String, TypeAnalyzer> typeAnalyzers) throws Exception {
		// Read in all the data and stored into temp file locally
		HeaderRow headerRow = null;
		int columnSize = 0;
		long rowCount = 0;
		try (CSVPrinter out = CommonUtils.createCSVPrinter(tempFile)) {
			for (String dataFileName : dataFileNames) {
				System.out.println("Processing " + dataFileName);
				try (CsvFileDataReader dataReader = new CsvFileDataReader(new File(dataFileName))) {
					if (getCsvDelimiter() != null) {
						dataReader.setCsvDelimiter(getCsvDelimiter());
					}
					
					if (getCsvDelimiterRegex() != null) {
						dataReader.setCsvDelimiterRegex(getCsvDelimiterRegex());
					}
					
					HeaderRow hr = dataReader.getHeaderRow();
					if (headerRow == null) {
						headerRow = hr;
						out.printRecord(headerRow.getHeaderNames());
						columnSize = headerRow.size();
					}
					else {
						if (columnSize != hr.size()) {
							throw new ProcessorException("Column size is not matched. (expected "
									+ columnSize + ", actual " + hr.size());
						}
					}

					DataRow dataRow = null;

					while ((dataRow = dataReader.readDataRow()) != null) {
						for (int i = 0; i < headerRow.getHeaderNames().size(); i++) {
							String headerName = headerRow.getHeaderNames().get(i);
							TypeAnalyzer typeAnalyzer = typeAnalyzers.get(headerName);
							if (typeAnalyzer == null) {
								typeAnalyzer = new TypeAnalyzer(CommonUtils.normalizeColumnName(headerName), i);
								typeAnalyzers.put(headerName, typeAnalyzer);
							}

							String value = dataRow.getValue(headerName);
							typeAnalyzer.analyze(value);
						}

						out.printRecord(dataRow.getValues());
						rowCount++;
					}

					out.flush();
					System.out.println("Successfully processed " + dataFileName);
				}
				catch (Exception e) {
					System.out.println("Failed to process " + dataFileName);
					throw e;
				}
			}
		}

		System.out.println("Finished data analysis and processed " + rowCount + " rows");

		return headerRow;
	}

	public String getCsvDelimiter() {
		return csvDelimiter;
	}

	public void setCsvDelimiter(String csvDelimiter) {
		this.csvDelimiter = csvDelimiter;
	}

	public String getCsvDelimiterRegex() {
		return csvDelimiterRegex;
	}

	public void setCsvDelimiterRegex(String csvDelimiterRegex) {
		this.csvDelimiterRegex = csvDelimiterRegex;
	}
}
