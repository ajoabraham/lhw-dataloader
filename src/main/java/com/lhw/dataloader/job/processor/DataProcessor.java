package com.lhw.dataloader.job.processor;

public interface DataProcessor {
	public void process() throws ProcessorException;
}
