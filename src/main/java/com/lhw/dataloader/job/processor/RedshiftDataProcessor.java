package com.lhw.dataloader.job.processor;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.job.publisher.DataPublisher;
import com.lhw.dataloader.job.publisher.RedshiftDataPublisher;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

/**
 * RedshiftDataProcessor is similar to LocalDataProcessor. The difference is
 * RedshiftDataProcess will output Redshift COPY command ready CSV files first. Then
 * copy all those CSV files into Amazon S3, then issue a COPY command in Redshift database
 * 
 * @author Tai Hu
 *
 */
public class RedshiftDataProcessor extends LocalDataProcessor {
	private static final Logger logger = LoggerFactory.getLogger(RedshiftDataProcessor.class.getName());

	private LoadMode loadMode = LoadMode.APPEND;
	
	public RedshiftDataProcessor(String tableName, LoadMode loadMode, String... dataFileNames) {
		super(tableName, dataFileNames);
		
		this.loadMode = loadMode;
	}

	@Override
	public void process() throws ProcessorException {
		try {
			File tempFile = new File(CommonUtils.generateFileName(tableName));
			Map<String, TypeAnalyzer> typeAnalyzers = new LinkedHashMap<>();
			HeaderRow headerRow = pullAndAnalyzeData(tempFile, typeAnalyzers);

			DataPublisher dataPublisher = new RedshiftDataPublisher(tableName, headerRow, typeAnalyzers, tempFile, loadMode);
			dataPublisher.publish();
			
			try {
				tempFile.delete();
				System.out.println("Successfully deleted " + tempFile.getAbsolutePath());
			}
			catch (Exception e) {
				System.out.println("Failed to delete " + tempFile.getAbsolutePath());
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ProcessorException(e.getMessage(), e);
		}
	}
}
