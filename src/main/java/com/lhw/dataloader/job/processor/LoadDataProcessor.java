package com.lhw.dataloader.job.processor;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.job.publisher.DataPublisher;
import com.lhw.dataloader.job.publisher.DbAppendDataPublisher;
import com.lhw.dataloader.job.publisher.DbReplaceDataPublisher;
import com.lhw.dataloader.job.publisher.DbUpsertDataPublisher;
import com.lhw.dataloader.job.reader.DataReader;
import com.lhw.dataloader.job.reader.RestDataReader;
import com.lhw.dataloader.model.DataRow;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

/**
 * This is a generic data loader, LoadDataProcessor reads data from Microstrategy server based on
 * start date, end date, and loading interval. Then if the target table does not exist, create table
 * and load all data into it. Otherwise, upsert the data into existing table.
 * 
 * @author Tai Hu
 *
 */
public class LoadDataProcessor implements DataProcessor {
	private static final Logger logger = LoggerFactory.getLogger(LoadDataProcessor.class.getName());

	private String[] reportIds = null;
	private String tableName = null;
	private String keyColumnName = null;
	private Date startDate = null;
	private Date endDate = null;
	private int loadInterval = 1;
	private String sessionStateUrl = null;
	private String dataUrlTemplate = null;
	private LoadMode loadMode = LoadMode.REPLACE;
	private boolean isLcEnrollments = false;
	private boolean ignoreLastColumn = false;

	private LoadDataProcessor(LoadDataProcessorBuilder builder) {
		this.sessionStateUrl = builder.sessionStateUrl;
		this.dataUrlTemplate = builder.dataUrlTemplate.replace(CommonConstants.PARAM_DELIMITER, 
				CommonUtils.encodeUrl(AppProps.I.getCsvDelimiter()));
		this.tableName = builder.tableName;
		this.keyColumnName = builder.keyColumnName;
		this.startDate = builder.startDate;
		this.endDate = builder.endDate;
		this.loadInterval = builder.loadInterval;
		this.reportIds = builder.reportIds;
		this.loadMode = builder.loadMode;
		this.isLcEnrollments = builder.isLcEnrollments;
		this.ignoreLastColumn = builder.ignoreLastColumn;
	}

	@Override
	public void process() throws ProcessorException {
		try {
			File tempFile = new File(CommonUtils.generateFileName(tableName));
			Map<String, TypeAnalyzer> typeAnalyzers = new LinkedHashMap<>();
			HeaderRow headerRow = pullAndAnalyzeData(tempFile, typeAnalyzers);

			if (loadMode == LoadMode.REPLACE || !CommonUtils.doesTableExist(tableName)) {
				DataPublisher dataPublisher = new DbReplaceDataPublisher(tableName, headerRow, typeAnalyzers, tempFile);
				dataPublisher.publish();	
			}
			else if (loadMode == LoadMode.APPEND) {
				DataPublisher dataPublisher = new DbAppendDataPublisher(tableName, headerRow, typeAnalyzers, tempFile);
				dataPublisher.publish();	
			}
			else {
				DataPublisher dataPublisher = new DbUpsertDataPublisher(tableName, keyColumnName, headerRow, typeAnalyzers, tempFile);
				dataPublisher.publish();
			}
		}
		catch (Exception e) {
			throw new ProcessorException(e.getMessage(), e);
		}
	}

	public HeaderRow pullAndAnalyzeData(File tempFile, Map<String, TypeAnalyzer> typeAnalyzers) throws Exception {
		// Read in all the data and stored into temp file locally
		HeaderRow headerRow = null;
		long rowCount = 0;
		
		try (CSVPrinter out = CommonUtils.createCSVPrinter(tempFile)) {
			for (String reportId : reportIds) {
				Date start = CommonUtils.copyDate(startDate);
				Date end = CommonUtils.copyDate(endDate);
				
				while (!start.after(end)) {
					int diff = (int) CommonUtils.daysInBetween(start, end);

					if (diff > loadInterval) {
						diff = loadInterval;
					}

					// If this is lc enrollments refresh, not need to load as interval
					Date intervalEnd = isLcEnrollments ? CommonUtils.copyDate(end) : DateUtils.addDays(start, diff - 1);
					String interval = isLcEnrollments ? CommonUtils.createInterval(start, start) 
							: CommonUtils.createInterval(start, intervalEnd);
					System.out.println("Loading " + interval);
					
					String dataUrl = dataUrlTemplate.replace(CommonConstants.PARAM_REPORT_ID, reportId)
							.replace(CommonConstants.PARAM_INTERVAL, CommonUtils.encodeUrl(interval));
					
					int maxRetry = AppProps.I.getMaxRetry();
					long retryInterval = AppProps.I.getRetryInterval() * 1000;
					int retryCount = 0;
					
					boolean isSuccessful = false;
					while (!isSuccessful && retryCount < maxRetry) {
						if (retryCount > 0) {
							System.out.println("Start retry " + retryCount + " for " + interval);
						}
						
    					try (DataReader dataReader = new RestDataReader(sessionStateUrl, dataUrl, ignoreLastColumn)) {
    						HeaderRow hr = dataReader.getHeaderRow();
    						if (headerRow == null) {
    							headerRow = hr;
    							out.printRecord(headerRow.getHeaderNames());
    							out.flush();
    						}
    
    						DataRow dataRow = null;
    						List<DataRow> dataRows = new ArrayList<>();
    						
    						while ((dataRow = dataReader.readDataRow()) != null) {
    							for (int i = 0; i < headerRow.getHeaderNames().size(); i++) {
    								String headerName = headerRow.getHeaderNames().get(i);
    								TypeAnalyzer typeAnalyzer = typeAnalyzers.get(headerName);
    								if (typeAnalyzer == null) {
    									typeAnalyzer = new TypeAnalyzer(CommonUtils.normalizeColumnName(headerName), i);
    									typeAnalyzers.put(headerName, typeAnalyzer);
    								}
    
    								String value = dataRow.getValue(headerName);
    								typeAnalyzer.analyze(value);
    							}
    
    							dataRows.add(dataRow);
    						}
    
    						for (DataRow r : dataRows) {
    							out.printRecord(r.getValues());
    						}
    						
    						out.flush();
    						System.out.println("Successfully pull and stored " + dataRows.size() + " rows for " + interval);
    						rowCount += dataRows.size();
    						isSuccessful = true;
    					}
    					catch (Exception e) {
    						logger.error(e.getMessage(), e);
    						
    						if (retryCount++ < maxRetry) {
    							long waitingPeriod = retryInterval * retryCount;
    							System.out.println("Failed to pull the data for " + interval + " and wait for " + waitingPeriod + "ms to retry");
    							
    							try {
    								Thread.sleep(waitingPeriod);
    							}
    							catch (Exception ex) {
    								logger.error(ex.getMessage(), ex);
    							}
    						}
    					}
					}

					if (isSuccessful) {
						start = DateUtils.addDays(intervalEnd, 1);
					}
					else {
						String message = "Data pull process was failed after " + retryCount + " retries. All data before "
								+ interval + " are saved into local file (total of " + rowCount 
								+ " rows, not including header row). Need to reload data starting from " + interval;
						System.out.println(message);
						throw new ProcessorException(message);
					}
				}
			}
		}

		System.out.println("Successfully pull and analyzed " + rowCount + " rows.");
		
		return headerRow;
	}
	
	public static final class LoadDataProcessorBuilder {
		private String[] reportIds = null;
		private String tableName = null;
		private String keyColumnName = null;
		private Date startDate = null;
		private Date endDate = null;
		private int loadInterval = 1;
		private String sessionStateUrl = null;
		private String dataUrlTemplate = null;
		private LoadMode loadMode = LoadMode.REPLACE;
		private boolean isLcEnrollments = Boolean.FALSE;
		private boolean ignoreLastColumn = Boolean.FALSE;
		
		private LoadDataProcessorBuilder() {			
		}
		
		public static LoadDataProcessorBuilder create() {
			return new LoadDataProcessorBuilder();
		}
		
		public LoadDataProcessorBuilder setSessionStateUrl(String sessionStateUrl) {
			this.sessionStateUrl = sessionStateUrl;
			return this;
		}
		
		public LoadDataProcessorBuilder setDataUrlTemplate(String dataUrlTemplate) {
			this.dataUrlTemplate = dataUrlTemplate;
			return this;
		}
		
		public LoadDataProcessorBuilder setStartDate(Date startDate) {
			this.startDate = startDate;
			return this;
		}
		
		public LoadDataProcessorBuilder setEndDate(Date endDate) {
			this.endDate = endDate;
			return this;
		}
		
		public LoadDataProcessorBuilder setLoadInterval(int loadInterval) {
			this.loadInterval = loadInterval;
			return this;
		}
		
		public LoadDataProcessorBuilder setTableName(String tableName) {
			this.tableName = tableName;
			return this;
		}
		
		public LoadDataProcessorBuilder setKeyColumnName(String keyColumnName) {
			this.keyColumnName = keyColumnName;
			return this;
		}
		
		public LoadDataProcessorBuilder setReportIds(String... reportIds) {
			this.reportIds = reportIds;
			return this;
		}
		
		public LoadDataProcessorBuilder setLoadMode(LoadMode loadMode) {
			this.loadMode = loadMode;
			return this;
		}
		
		public LoadDataProcessorBuilder setLcEnrollments(boolean lcEnrollments) {
			this.isLcEnrollments = lcEnrollments;
			return this;
		}
		
		public LoadDataProcessorBuilder setIgnoreLastColumn(boolean ignoreLastColumn) {
			this.ignoreLastColumn = ignoreLastColumn;
			return this;
		}
		
		public LoadDataProcessor build() {
			if (startDate == null || endDate == null || startDate.after(endDate)) {
				throw new IllegalArgumentException("Invalid start date or end date.");
			}
			else if (CommonUtils.isBlank(tableName)) {
				throw new IllegalArgumentException("Table name cannot be empty.");
			}
			else if (CommonUtils.isBlank(keyColumnName)) {
				throw new IllegalArgumentException("Key column name cannot be empty.");
			}
			else if (reportIds == null || reportIds.length == 0) {
				throw new IllegalArgumentException("Report IDS cannot be empty.");
			}
			else if (CommonUtils.isBlank(sessionStateUrl)) {
				throw new IllegalArgumentException("Session state URL cannot be empty.");
			}
			else if (CommonUtils.isBlank(dataUrlTemplate)) {
				throw new IllegalArgumentException("Data URL template cannot be empty.");
			}
			
			return new LoadDataProcessor(this);
		}
	}
}
