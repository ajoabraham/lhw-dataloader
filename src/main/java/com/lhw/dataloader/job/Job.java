package com.lhw.dataloader.job;

import java.util.concurrent.Callable;

public interface Job<T> extends Callable<T> {

}
