package com.lhw.dataloader.job;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.lhw.dataloader.common.AppProps;

public enum JobManager {
	I;
	
	private ScheduledExecutorService threadPool = null;
	
	private JobManager() {		
		threadPool = Executors.newScheduledThreadPool(AppProps.I.getThreadPoolSize());
	}
	
	public <T> Future<T> execute(Job<T> job) {
		return threadPool.submit(job);
	}
	
	// Run once at specific times
	public <T> Future<T> schedule(Job<T> job, Date startTime) {
		long delay = startTime.getTime() - System.currentTimeMillis();
		return threadPool.schedule(job, delay, TimeUnit.MILLISECONDS);
	}
	
	public Future<?> schedule(Job<?> job, Date firstTime, long period) {
		long initialDelay = firstTime.getTime() - System.currentTimeMillis();
		Runnable r = () -> {
			try {
				job.call();
			}
			catch (Exception e) {
			}
		};
		
		return threadPool.scheduleAtFixedRate(r, initialDelay, period * 60 * 1000, TimeUnit.MILLISECONDS);
	}
	
	public void shutdown() {
		threadPool.shutdown();
	}
}
