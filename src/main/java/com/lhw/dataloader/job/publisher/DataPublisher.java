package com.lhw.dataloader.job.publisher;

public interface DataPublisher {
	public void publish() throws PublisherException;
}
