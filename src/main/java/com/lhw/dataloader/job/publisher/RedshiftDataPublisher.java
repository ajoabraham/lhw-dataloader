package com.lhw.dataloader.job.publisher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.common.DataType;
import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.model.DataTypeInfo;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.AmazonS3Service;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.ConnUtils;
import com.lhw.dataloader.util.DdlGenerator;
import com.lhw.dataloader.util.TypeAnalyzer;

/**
 * Output data into Redshift COPY command ready CSV files. Split the file based on the redshiftReadyDataFileSize
 * parameter defined in application.properties file. Also stored final DDL into a local file.
 * 
 * @author Tai Hu
 *
 */
public class RedshiftDataPublisher extends AbstractDataPublisher {
	private static final Logger logger = LoggerFactory.getLogger(RedshiftDataPublisher.class.getName());
	
	private static final String COPY_COMMAND_BASE = "copy %s from 's3://%s' credentials 'aws_access_key_id=%s;aws_secret_access_key=%s' gzip removequotes";
		
	private String tableName = null;
	private File dataFile = null;
	private Map<String, TypeAnalyzer> typeAnalyzers = null;
	// This header row contains header in the order of current input csv file.
	private HeaderRow headerRow = null;
	private LoadMode loadMode = LoadMode.APPEND;
	private String outputFileNamePrefix = UUID.randomUUID().toString();
	private String outputFileNameTemplate = outputFileNamePrefix + "_%d.csv.gzip";
	private String keyColumnName = "booking_date";
	
	public RedshiftDataPublisher(String tableName, HeaderRow headerRow, 
			Map<String, TypeAnalyzer> typeAnalyzers, File dataFile, LoadMode loadMode) {
		this.tableName = tableName;
		this.typeAnalyzers = typeAnalyzers;
		this.headerRow = headerRow;
		this.dataFile = dataFile;
		this.loadMode = loadMode;
	}

	@Override
	public void publish() throws PublisherException {		
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {						
			
			// If force reload or table does not exist
			List<DataTypeInfo> currentTypeInfos = CommonUtils.toDataTypeInfos(headerRow, typeAnalyzers);
			if (loadMode == LoadMode.REPLACE || !CommonUtils.doesTableExist(tableName)) {
				createOrReplaceTable(statement, tableName, currentTypeInfos);
			}
			else {
				currentTypeInfos = updateSchema(statement, tableName, currentTypeInfos);
			}
			
			long startTime = System.currentTimeMillis();
			List<File> outputFiles = generateRedshiftReadyData(currentTypeInfos);
			logger.info("It took " + (System.currentTimeMillis() - startTime) + "ms to generate Redshift ready data files.");
			
			try {
				outputSchemaInfoToLocalFile(currentTypeInfos);
			}
			catch (FileNotFoundException e) {
				throw new PublisherException(e.getMessage(), e);
			}
			
			startTime = System.currentTimeMillis();
			List<String> uploadedObjectKeys = uploadFilesToS3(outputFiles);
			logger.info("It took " + (System.currentTimeMillis() - startTime) + "ms to upload all Redshift ready data files into S3.");
			
			// Execute COPY command
//			String fileNamePrefix = StringUtils.getCommonPrefix(outputFiles.stream().map(File::getName).collect(Collectors.toList()).toArray(new String[0]));
			String objectPath = AppProps.I.getBucketName() + "/";
			String objectKeyPrefix = AppProps.I.getObjectKeyPrefix();
			if (CommonUtils.isBlank(objectKeyPrefix)) {
				objectPath += outputFileNamePrefix;
			}
			else if (objectKeyPrefix.endsWith("/")) {
				objectPath = objectPath + objectKeyPrefix + outputFileNamePrefix;
			}
			else {
				objectPath = objectPath + objectKeyPrefix + "/" + outputFileNamePrefix;
			}
			
			String stageTableName = null;
			if (loadMode == LoadMode.UPSERT) {
				stageTableName = CommonUtils.getStageTableName(tableName);
				String ddl = "create temp table " + stageTableName + " (like " + tableName + ")";
				logger.debug("Stage table DDL = {}", ddl);
				
				// create stage table
				statement.execute(ddl);
			}
					
			String copyCommand = String.format(COPY_COMMAND_BASE, 
					loadMode == LoadMode.UPSERT ? stageTableName : tableName, 
					objectPath, AppProps.I.getAccessKeyId(), AppProps.I.getAccessKey());
			
			if (!CommonUtils.isBlank(AppProps.I.getRegion())) {
				copyCommand += " region '" + AppProps.I.getRegion() + "'";
			}
			
			logger.info("COPY command = " + copyCommand);
			
			startTime = System.currentTimeMillis();
			statement.execute(copyCommand);
			logger.info("It took " + (System.currentTimeMillis() - startTime) + "ms to copy data into " 
					+ (loadMode == LoadMode.UPSERT ? stageTableName : tableName));
			
			if (loadMode == LoadMode.UPSERT) {
				// Upsert into main table
				String deleteQuery = "delete from " + tableName
						+ " where " + tableName + "." + keyColumnName + " >= (select min(" + stageTableName + "." + keyColumnName
						+ ") from " + stageTableName + ")";
				logger.debug("Delete query = {}", deleteQuery);
				int deletedRows = statement.executeUpdate(deleteQuery);
				logger.info("Deleted " + deletedRows + " from " + tableName + "(Rows will be updated from staging table)");
				
				// Insert all rows from staging table into original table
				String insertQuery = "insert into " + tableName + " select * from " + stageTableName;
				logger.debug("Insert query = {}", insertQuery);
				statement.execute(insertQuery);
				conn.commit();
				logger.info("Merged staging table with " + tableName);
				conn.setAutoCommit(true);
			
				// Drop staging table
				statement.execute("drop table " + stageTableName);
				logger.info("Dropped staging table " + stageTableName);
			}
			
			// Clean up data file
			for (File file : outputFiles) {
				try {
					file.delete();
					logger.info("Successfully deleted {}", file.getAbsolutePath());
				}
				catch (Exception e) {
					logger.info("Failed to delete {}", file.getAbsolutePath());
				}
			}
			
			// Clean up S3
			if (uploadedObjectKeys != null) {
	    		AmazonS3Service s3 = AmazonS3Service.newInstance(AppProps.I.getAccessKeyId(), AppProps.I.getAccessKey());
	    		for (String objectKey : uploadedObjectKeys) {
	    			try {
	    				s3.deleteObject(AppProps.I.getBucketName(), objectKey);
	    				logger.info("Successfully deleted {} from S3", objectKey);
	    			}
	    			catch (Exception e) {
	    				logger.info("Failed to delete {} from S3", objectKey);
	    			}
	    		}
			}
		}
		catch (PublisherException e) {
			throw e;
		}
		catch (Exception e) {
			throw new PublisherException(e.getMessage(), e);
		}
	}
	
	private List<String> uploadFilesToS3(List<File> outputFiles) throws Exception {
		List<String> uploadedObjectKeys = new ArrayList<>();
		
		if (CommonUtils.isBlank(AppProps.I.getAccessKeyId())) {
			throw new PublisherException("Amazon S3 Access Key ID is empty.");
		}
		
		if (CommonUtils.isBlank(AppProps.I.getAccessKey())) {
			throw new PublisherException("Amazon S3 Access Key is empty.");
		}
		
		if (CommonUtils.isBlank(AppProps.I.getBucketName())) {
			throw new PublisherException("Amazon S3 Bucket name is empty.");
		}
		
		AmazonS3Service s3 = AmazonS3Service.newInstance(AppProps.I.getAccessKeyId(), AppProps.I.getAccessKey());
		
		for (File file : outputFiles) {
			String objectKey = AppProps.I.getObjectKeyPrefix();
			if (CommonUtils.isBlank(objectKey)) {
				objectKey = file.getName();
			}
			else if (objectKey.endsWith("/")) {
				objectKey += file.getName();
			}
			else {
				objectKey = objectKey + "/" + file.getName();
			}
			
			s3.uploadFile(AppProps.I.getBucketName(), objectKey, file);
			uploadedObjectKeys.add(objectKey);
			logger.info("Successfully uploaded " + file.getAbsolutePath() + " onto S3 bucket " + AppProps.I.getBucketName() + " under " + objectKey);
		}
		
		return uploadedObjectKeys;
	}
	
	private List<File> generateRedshiftReadyData(List<DataTypeInfo> dataTypeInfos) throws PublisherException, IOException {
		List<File> outputFiles = new ArrayList<>();
		
		CSVParser csvParser = null;
		try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(dataFile), CommonConstants.DEFAULT_ENCODING_NAME))) {						
			// Skip the header line
			in.readLine();
			// Use the incoming CSV file header name order here
			csvParser = new CSVParser(in, CSVFormat.newFormat(AppProps.I.getCsvDelimiter().charAt(0))
					.withHeader(headerRow.getHeaderNames().stream().map(n -> CommonUtils.normalizeColumnName(n))
							.collect(Collectors.toList()).toArray(new String[0]))
					.withQuote(AppProps.I.getCsvQuote().charAt(0))
					.withIgnoreEmptyLines(true).withAllowMissingColumnNames(false));

			long redshiftReadyDataFileSize = AppProps.I.getRedshiftReadyDataFileSize();
			long rowCount = 0;
			int fileCount = 0;
			File file = generateOutputFile(fileCount++);
			outputFiles.add(file);
			CSVPrinter out = createCSVPrinter(file);
			
			for (CSVRecord r : csvParser) {		
				List<String> values = new ArrayList<>();
				for (DataTypeInfo dataTypeInfo : dataTypeInfos) {
					String value = r.get(dataTypeInfo.getColumnName());
					DataType dataType = dataTypeInfo.getDataType();
					
					if (CommonUtils.isBlank(value)) {
						values.add("");
					}
					else if (dataType == DataType.DATE) {
						values.add(CommonUtils.convertToDefaultDateFormat(value));
					}
					else if (dataType == DataType.TIMESTAMP) {
						values.add(CommonUtils.convertToDefaultTimestampFormat(value));
					}
					else {
						values.add(CommonUtils.escapeForCopyCommand(value));
					}
					
//					if (i != (headerRow.size() - 1)) {
//						values.append(AppProps.I.getCsvDelimiter());
//					}
				}
				
				if (rowCount >= redshiftReadyDataFileSize) {
					out.flush();
					out.close();
					logger.info("Successfully created CSV file with " + rowCount + " rows");
					file = generateOutputFile(fileCount++);
					outputFiles.add(file);
					out = createCSVPrinter(file);
					rowCount = 0;
				}
				
				out.printRecord(values);
				rowCount++;				
			}
			
			out.flush();
			out.close();
			logger.info("Successfully created last CSV file with " + rowCount + " rows");
		}
		finally {
			if (csvParser != null) {
				try {
					csvParser.close();
				}
				catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		
		return outputFiles;
	}
	
	private void outputSchemaInfoToLocalFile(List<DataTypeInfo> dataTypeInfos) throws FileNotFoundException, PublisherException {
		String ddl = DdlGenerator.generateDdl(tableName, dataTypeInfos);
		logger.debug("DDL = {}", ddl);
		try (PrintWriter out = new PrintWriter(new FileOutputStream(tableName + "_ddl.sql"))) {
			out.print(ddl);
		}
				
		logger.info("Successfully output current schema information.");
	}
	
	private CSVPrinter createCSVPrinter(File file) throws IOException {
		logger.info("Creating " + file.getAbsolutePath());
		GZIPOutputStream gzipOutputStream = new GZIPOutputStream(new FileOutputStream(file));
		return CommonUtils.createCSVPrinter(new PrintWriter(gzipOutputStream, true));
//		return new PrintWriter(file, CommonConstants.DEFAULT_ENCODING_NAME);
	}
	
	private File generateOutputFile(int fileCount) {
//		return new File(tableName + "_" + fileCount + ".csv.gzip");
		return new File(String.format(outputFileNameTemplate, fileCount));
	}
}
