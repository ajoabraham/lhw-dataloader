package com.lhw.dataloader.job.publisher;

import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.model.DataTypeInfo;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.ConnUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

/**
 * DbUpsertDataPublisher will upsert data into existing table
 * 
 * @author Tai Hu
 *
 */
public class DbUpsertDataPublisher extends DbAppendDataPublisher {
	private static final Logger logger = LoggerFactory.getLogger(DbUpsertDataPublisher.class.getName());

	private String keyColumnName = null;

	public DbUpsertDataPublisher(String tableName, String keyColumnName, HeaderRow headerRow, Map<String, TypeAnalyzer> typeAnalyzers, File dataFile) {
		super(tableName, headerRow, typeAnalyzers, dataFile);
		this.keyColumnName = keyColumnName;
	}

	@Override
	public void publish() throws PublisherException {
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			
			// update schema
			List<DataTypeInfo> currentTypeInfos = CommonUtils.toDataTypeInfos(headerRow, typeAnalyzers);
			List<DataTypeInfo> dataTypeInfos = updateSchema(statement, tableName, currentTypeInfos);			
			String stageTableName = CommonUtils.getStageTableName(tableName);
			String ddl = "create temp table " + stageTableName + " (like " + tableName + ")";
			logger.debug("Stage table DDL = {}", ddl);
			
			// create stage table
			statement.execute(ddl);

			insertIntoTable(conn, statement, dataFile, stageTableName, headerRow, dataTypeInfos);
		
			// Upsert into main table
			String deleteQuery = "delete from " + tableName + " using " + stageTableName
					+ " where " + tableName + "." + keyColumnName + "=" + stageTableName + "." + keyColumnName;
			logger.debug("Delete query = {}", deleteQuery);
			int deletedRows = statement.executeUpdate(deleteQuery);
			System.out.println("Deleted " + deletedRows + " from " + tableName + "(Rows will be updated from staging table)");
			
			// Insert all rows from staging table into original table
			String insertQuery = "insert into " + tableName + " select * from " + stageTableName;
			logger.debug("Insert query = {}", insertQuery);
			statement.execute(insertQuery);
			conn.commit();
			System.out.println("Merged staging table with " + tableName);
			conn.setAutoCommit(true);
		
			// Drop staging table
			statement.execute("drop table " + stageTableName);
			System.out.println("Dropped staging table " + stageTableName);
			// Clean up resources
			statement.execute("vacuum " + tableName);
			statement.execute("analyze " + tableName);
			System.out.println("Vacuum and analyze Redshift database");
		}
		catch (PublisherException e) {
			throw e;
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new PublisherException(e.getMessage(), e);
		}
	}
}
