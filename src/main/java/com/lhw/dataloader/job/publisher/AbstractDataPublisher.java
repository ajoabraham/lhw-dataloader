package com.lhw.dataloader.job.publisher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.common.DataType;
import com.lhw.dataloader.model.DataTypeInfo;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.DdlGenerator;
import com.lhw.dataloader.util.TypeAnalyzer;

public abstract class AbstractDataPublisher implements DataPublisher {
	private static final Logger logger = LoggerFactory.getLogger(AbstractDataPublisher.class.getName());
	
	public AbstractDataPublisher() {
	}

//	private static final String VERO_SCHEMA_INFO_INSERT_QUERY = "insert into vero_schema_info (table_name, sqls, data_type_infos) "
//			+ "values (?, ?, ?)";
//	protected void updateVeroSchemaInfoTable(String tableName, String sqls, List<DataTypeInfo> dataTypeInfos) throws PublisherException {
//		try (Connection conn = ConnUtils.I.getConnection();
//				PreparedStatement statement = conn.prepareStatement(VERO_SCHEMA_INFO_INSERT_QUERY)) {
//			statement.setString(1, tableName);
//			statement.setString(2, sqls);
//			String jsonString = new ObjectMapper().writeValueAsString(dataTypeInfos);
//			statement.setString(3, jsonString);
//			statement.execute();
//		}
//		catch (SQLException | JsonProcessingException e) {
//			logger.error(e.getMessage(), e);
//			throw new PublisherException(e.getMessage(), e);
//		}
//	}
	
	private static final String BASE_INSERT_QUERY = "insert into %s (%s) values ";
	protected void insertIntoTable(Connection conn, Statement statement, File dataFile, String tableName, HeaderRow headerRow, List<DataTypeInfo> updatedDataTypeInfos)
			throws UnsupportedEncodingException, FileNotFoundException, IOException, SQLException, PublisherException {
		CSVParser csvParser = null;
		try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(dataFile), CommonConstants.DEFAULT_ENCODING_NAME))) {
			conn.setAutoCommit(false);

			// Create multi-row insert statement.
			StringBuffer columnNames = new StringBuffer();

			for (int i = 0; i < headerRow.size(); i++) {
				columnNames.append(CommonUtils.normalizeColumnName(headerRow.getHeaderNames().get(i)));
				if (i != (headerRow.size() - 1)) {
					columnNames.append(",");
				}
			}

			long startTime = System.currentTimeMillis();
			// Insert data
			String baseQuery = String.format(BASE_INSERT_QUERY, tableName, columnNames.toString());

			StringBuffer values = new StringBuffer(baseQuery);
			int batchInsertSize = AppProps.I.getBatchInsertSize();
			int count = 0;
			// Skip the header line
			in.readLine();
			csvParser = new CSVParser(in, CSVFormat.newFormat(AppProps.I.getCsvDelimiter().charAt(0))
					.withHeader(headerRow.getHeaderNames().toArray(new String[0])).withIgnoreEmptyLines(true)
					.withQuote(AppProps.I.getCsvQuote().charAt(0))
					.withAllowMissingColumnNames(false));
			for (CSVRecord r : csvParser) {
				values.append(count == 0 ? "(" : ",(");

				for (int i = 0; i < headerRow.size(); i++) {
					String value = r.get(headerRow.getHeaderNames().get(i));
					DataType dataType = updatedDataTypeInfos.get(i).getDataType();

					if (CommonUtils.isBlank(value)) {
						values.append("NULL");
					}
					else if (dataType == DataType.VARCHAR) {
						values.append("'").append(CommonUtils.escape(value)).append("'");
					}
					else if (dataType == DataType.DATE) {
						values.append("'").append(CommonUtils.convertToDefaultDateFormat(value)).append("'");
					}
					else if (dataType == DataType.TIMESTAMP) {
						values.append("'").append(CommonUtils.convertToDefaultTimestampFormat(value)).append("'");
					}
					else {
						values.append(value);
					}

					if (i != (headerRow.size() - 1)) {
						values.append(",");
					}
				}

				values.append(")");

				if (++count >= batchInsertSize) {
					System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to analyze " + count
							+ " rows.");
					startTime = System.currentTimeMillis();
					statement.execute(values.toString());
					System.out.println(
							"It took " + (System.currentTimeMillis() - startTime) + "ms to execute the query");
					startTime = System.currentTimeMillis();
					conn.commit();
					System.out.println(
							"It took " + (System.currentTimeMillis() - startTime) + "ms to commit the query");
					startTime = System.currentTimeMillis();
					values = new StringBuffer(baseQuery);
					count = 0;
				}
			}

			if (!baseQuery.equals(values.toString())) {
				System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to analyze last " + count
						+ " rows.");
				startTime = System.currentTimeMillis();
				statement.execute(values.toString());
				System.out.println(
						"It took " + (System.currentTimeMillis() - startTime) + "ms to execute the last query ("
								+ CommonUtils.getStringLength(values.toString()) + " bytes)");
				startTime = System.currentTimeMillis();
				conn.commit();
				System.out.println(
						"It took " + (System.currentTimeMillis() - startTime) + "ms to commit the last query");
			}
		}
		finally {
			if (csvParser != null) {
				try {
					csvParser.close();
				}
				catch (IOException e) {
					logger.error(e.getMessage(), e);
				}
			}
			
			conn.setAutoCommit(true);
		}
	}
	
	private static final String DROP_TABLE_QUERY = "drop table if exists %s";
	protected void createOrReplaceTable(Statement statement, String tableName, List<DataTypeInfo> dataTypeInfos) throws SQLException, PublisherException {
		// Drop table if it exists.
		statement.execute(String.format(DROP_TABLE_QUERY, tableName));
		// Create table
		String ddl = DdlGenerator.generateDdl(tableName, dataTypeInfos);
		logger.debug("DDL = {}", ddl);
		statement.execute(ddl);
		
//		updateVeroSchemaInfoTable(tableName, ddl, dataTypeInfos);
	}
	
	protected List<DataTypeInfo> updateSchema(Statement statement, String tableName, List<DataTypeInfo> currentTypeInfos) throws PublisherException, SQLException {
		List<DataTypeInfo> dataTypeInfos = new ArrayList<>();

		List<DataTypeInfo> previousTypeInfos = CommonUtils.getPreviousDataTypeInfos(tableName);

		// If column size is different throw the exception and stop the process.
		if (previousTypeInfos.size() != currentTypeInfos.size()) {
			throw new PublisherException("Current column size is different from existing column size. (existing: "
					+ previousTypeInfos.size() + ", current: " + currentTypeInfos.size());
		}

		StringBuffer alterTableSqls = new StringBuffer();

		// TH 11/26/2016, need to handle new column order is different from existing column
		// order.
		for (DataTypeInfo previous : previousTypeInfos) {
			DataTypeInfo current = currentTypeInfos.stream().filter(t -> previous.getColumnName().equals(t.getColumnName()))
					.findAny().orElse(null);

			if (current == null) {
				throw new PublisherException("Column name " + previous.getColumnName() + " does not exsit in CSV file.");
			}

//			if (previous.getColumnPosition() != current.getColumnPosition()) {
//				throw new PublisherException("Current column position is different from existing column position. (existing: "
//						+ previous.getColumnPosition() + ", current: " + current.getColumnPosition());
//			}

			DataTypeInfo typeInfo = TypeAnalyzer.mergeDataTypeInfo(previous, current);

			if (previous.getDataType() == typeInfo.getDataType()) {
				if (!previous.equals(typeInfo)) {
					// This means only size (length, precision or scale) is
					// different now.
					// Based on the logic in mergeDataTypeInfo() method,
					// typeInfo always hold
					// the correct type information. Database column need to be
					// updated to that
					// size.
					alterTableSqls.append(updateColumnType(statement, tableName, typeInfo));
				}
			}
			else if (previous.getDataType() == DataType.INTEGER) {
				if (typeInfo.getDataType() == DataType.BIGINT || typeInfo.getDataType() == DataType.DECIMAL) {
					alterTableSqls.append(updateColumnType(statement, tableName, typeInfo));
				}
			}
			else if (previous.getDataType() == DataType.BIGINT) {
				if (typeInfo.getDataType() == DataType.DECIMAL) {
					alterTableSqls.append(updateColumnType(statement, tableName, typeInfo));
				}
			}
			else {
				throw new PublisherException("Invalid data type change. (existing: "
						+ previous.getDataType().getSqlTypeName() + ", current: "
						+ current.getDataType().getSqlTypeName());
			}

			dataTypeInfos.add(typeInfo);
		}

//		if (alterTableSqls.length() > 0) {
//			// Type is changed, need to record the changes in vero_schema_info
//			// table
//			updateVeroSchemaInfoTable(tableName, alterTableSqls.toString(), dataTypeInfos);
//		}

		return dataTypeInfos;
	}

	private String updateColumnType(Statement statement, String tableName, DataTypeInfo typeInfo)
			throws PublisherException, SQLException {
		StringBuffer sqls = new StringBuffer();
		String tempColumnName = CommonUtils.getTempColumnName(typeInfo.getColumnName());

		// Add a temp column with latest type
		String addColumnQuery = "alter table " + tableName + " add column " + tempColumnName + " "
				+ typeInfo.generateTypeSql() + " default NULL";
		statement.execute(addColumnQuery);
		sqls.append(addColumnQuery).append(System.lineSeparator());

		// Copy old column into new column
		String copyQuery = "update " + tableName + " set " + tempColumnName + "=" + typeInfo.getColumnName();
		statement.execute(copyQuery);
		sqls.append(copyQuery).append(System.lineSeparator());

		// Drop old column
		String dropColumnQuery = "alter table " + tableName + " drop column " + typeInfo.getColumnName();
		statement.execute(dropColumnQuery);
		sqls.append(dropColumnQuery).append(System.lineSeparator());

		// Rename new column into old column name
		String renameColumnQuery = "alter table " + tableName + " rename column " + tempColumnName + " to "
				+ typeInfo.getColumnName();
		statement.execute(renameColumnQuery);
		sqls.append(renameColumnQuery);

		return sqls.toString();
	}

//	private static final String DATA_TYPE_INFO_QUERY = "select data_type_infos from vero_schema_info "
//			+ "where table_name = ? and id = (select max(v.id) from vero_schema_info v where v.table_name = ?)";
//
//	// Load latest data type info from database
//	private List<DataTypeInfo> getPreviousDataTypeInfos(String tableName) {
//		List<DataTypeInfo> dataTypeInfos = null;
//		try (Connection conn = ConnUtils.I.getConnection();
//				PreparedStatement statement = conn.prepareStatement(DATA_TYPE_INFO_QUERY)) {
//			statement.setString(1, tableName);
//			statement.setString(2, tableName);
//
//			ResultSet resultSet = statement.executeQuery();
//
//			if (resultSet.next()) {
//				String dataTypeInfoJson = resultSet.getString(1);
//				dataTypeInfos = new ObjectMapper().readValue(dataTypeInfoJson, new TypeReference<List<DataTypeInfo>>() {
//				});
//			}
//		}
//		catch (SQLException | IOException e) {
//			logger.error(e.getMessage(), e);
//		}
//
//		return dataTypeInfos;
//	}
}
