package com.lhw.dataloader.job.publisher;

import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.model.DataTypeInfo;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.ConnUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

/**
 * DbReplaceDataPublisher will create new table or replace existing table, and then insert all data into it. 
 * 
 * @author Tai Hu
 *
 */
public class DbReplaceDataPublisher extends AbstractDataPublisher {
	private static final Logger logger = LoggerFactory.getLogger(DbReplaceDataPublisher.class.getName());
	
	private String tableName = null;
	private File dataFile = null;
	private Map<String, TypeAnalyzer> typeAnalyzers = null;
	private HeaderRow headerRow = null;

	public DbReplaceDataPublisher(String tableName, HeaderRow headerRow, Map<String, TypeAnalyzer> typeAnalyzers, File dataFile) {
		this.tableName = tableName;
		this.typeAnalyzers = typeAnalyzers;
		this.headerRow = headerRow;
		this.dataFile = dataFile;
	}

	@Override
	public void publish() throws PublisherException {
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {

			List<DataTypeInfo> dataTypeInfos = CommonUtils.toDataTypeInfos(headerRow, typeAnalyzers);
			
			createOrReplaceTable(statement, tableName, dataTypeInfos);

			insertIntoTable(conn, statement, dataFile, tableName, headerRow, dataTypeInfos);
			
			statement.execute("vacuum " + tableName);
			statement.execute("analyze " + tableName);
			System.out.println("Vacuum and analyze Redshift database");
		}
		catch (PublisherException e) {
			throw e;
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new PublisherException(e.getMessage(), e);
		}
	}
}
