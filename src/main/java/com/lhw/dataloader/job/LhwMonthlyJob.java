package com.lhw.dataloader.job;

import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_REPORT_IDS;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_TABLE_NAME;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.job.JobStats.JobState;
import com.lhw.dataloader.job.processor.DimDataProcessor;
import com.lhw.dataloader.job.processor.LoadDataProcessor;
import com.lhw.dataloader.job.processor.LoadDataProcessor.LoadDataProcessorBuilder;
import com.lhw.dataloader.job.processor.ProcessorException;
import com.lhw.dataloader.job.processor.RedshiftDataProcessor;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

public class LhwMonthlyJob implements Job<JobStats> {
	private static final Logger logger = LoggerFactory.getLogger(LhwMonthlyJob.class.getName());
	
	private static final int MAX_RETRY = 3;
	private static final long RETRY_INTERVAL = 1000 * 60 * 5;
	
	public LhwMonthlyJob() {
	}
	
	@Override
	public JobStats call() throws Exception {
		JobStats jobStats = new JobStats();
		jobStats.setName("LHW Monthly Data Refresh");
		jobStats.setStartTime(new Date());
		
		logger.info("LHW Monthly Data Refresh started on " + jobStats.getStartTime().toString());
		
		// Step 1. Pull booking_mstr_dim data
		// Step 2. Load booking_mstr_dim data
		// Step 3. Pull booking_status_dim data
		// Step 4. Load booking_status_dim data
		// Step 5. Drop lc_enrollments view
		// Step 6. Load lc_entrollments data
		// Step 7. Recreate lc_enrollments view
		// Step 8. Grant all permission
		
		logger.info("######## 1. Start pulling {} data ########", BOOKING_MSTR_DIM_TABLE_NAME);
		long startTime = System.currentTimeMillis();
		pullAndAnalyzeData(BOOKING_MSTR_DIM_TABLE_NAME, BOOKING_MSTR_DIM_KEY_COLUMN, BOOKING_MSTR_DIM_REPORT_ID, AppProps.I.getDataLoadInterval());		
		String[] dimFileNames = {renameFile(BOOKING_MSTR_DIM_TABLE_NAME)};
		logger.info("######## Successfully finished pulling {} ({}ms) ########", BOOKING_MSTR_DIM_TABLE_NAME, (System.currentTimeMillis() - startTime));
		
		logger.info("######## 2. Start loading {} data ########", BOOKING_MSTR_DIM_TABLE_NAME);
		startTime = System.currentTimeMillis();
		process(BOOKING_MSTR_DIM_TABLE_NAME, LoadMode.REPLACE, dimFileNames);
		logger.info("######## Successfully finished loading {} ({}ms) ########", BOOKING_MSTR_DIM_TABLE_NAME, (System.currentTimeMillis() - startTime));
		
		logger.info("######## 3. Start pulling {} data ########", BOOKING_STATUS_DIM_TABLE_NAME);
		startTime = System.currentTimeMillis();
		pullAndAnalyzeData(BOOKING_STATUS_DIM_TABLE_NAME, BOOKING_STATUS_DIM_KEY_COLUMN, BOOKING_STATUS_DIM_REPORT_ID, 180);
		String[] statusFileNames = {renameFile(BOOKING_STATUS_DIM_TABLE_NAME)};
		logger.info("######## Successfully finished pulling {} ({}ms) ########", BOOKING_STATUS_DIM_TABLE_NAME, (System.currentTimeMillis() - startTime));
		
		logger.info("######## 4. Start loading {} data ########", BOOKING_STATUS_DIM_TABLE_NAME);
		startTime = System.currentTimeMillis();
		process(BOOKING_STATUS_DIM_TABLE_NAME, LoadMode.REPLACE, statusFileNames);
		logger.info("######## Successfully finished loading {} ({}ms) ########", BOOKING_STATUS_DIM_TABLE_NAME, (System.currentTimeMillis() - startTime));
		
		logger.info("######## 5. Start dropping lc_enrollments view ########");
		startTime = System.currentTimeMillis();
		CommonUtils.dropViews();
		logger.info("######## Successfully finished dropping lc_enrollments view ({}ms) ########", (System.currentTimeMillis() - startTime));
		
		boolean isSucceeded = false;
		int retryCount = 0;
		
		while (retryCount++ < MAX_RETRY && !isSucceeded) {
			try {
        		logger.info("######## 6. Start loading {} data ########", LC_ENROLLMENTS_TABLE_NAME);
        		startTime = System.currentTimeMillis();
        		DimDataProcessor proc = new DimDataProcessor(AppProps.I.getSessionStateUrl(), 
        				AppProps.I.getDimDataUrlTemplate(), LC_ENROLLMENTS_TABLE_NAME, LC_ENROLLMENTS_REPORT_IDS);
        		proc.process();
        		logger.info("######## Successfully finished loading {} ({}ms) ########", LC_ENROLLMENTS_TABLE_NAME, (System.currentTimeMillis() - startTime));
        		
        		isSucceeded = true;
			}
			catch(Exception e) {
				logger.error(e.getMessage(), e);
				// Wait 5 minutes before retry
				Thread.sleep(RETRY_INTERVAL);
				logger.info("Start retry {}", retryCount);
			}
		}
		
		if (!isSucceeded) {
			logger.info("Tried step 6 {} times and failed.", MAX_RETRY);
			throw new Exception("Tried step 6 " + MAX_RETRY + " and failed.");
		}
		
		logger.info("######## 7. Start recreating lc_enrollments view ########");
		startTime = System.currentTimeMillis();
		CommonUtils.recreateViews();
		logger.info("######## Successfully finished recreating lc_enrollments view ({}ms) ########", (System.currentTimeMillis() - startTime));
		
		logger.info("######## 8. Start granting all permissions ########");
		startTime = System.currentTimeMillis();
		CommonUtils.grantPermission();
		logger.info("######## Successfully finished granting all permission ({}ms) ########", (System.currentTimeMillis() - startTime));
		
		jobStats.setEndTime(new Date());
		jobStats.setState(JobState.SUCCEEDED);
		
		String msg = null;
		if (jobStats.isSucceeded()) {
			msg = "Successfully completed all jobs, started on " + jobStats.getStartTime().toString()
					+ " and ended on " + jobStats.getEndTime().toString();
		}
		else {
			msg = "Failed to completed all jobs, please check below for more details. Job started on "
					+ jobStats.getStartTime().toString() + " and ended on " + jobStats.getEndTime().toString();
		}
		
		jobStats.addMessage(msg);		
		logger.info(msg);
		logger.info(jobStats.buildEmailBody());

		return jobStats;
	}
	
	private void pullAndAnalyzeData(String tableName, String keyColumnName, String reportId, int loadInterval) throws Exception {
		LoadDataProcessor proc = LoadDataProcessorBuilder.create().setSessionStateUrl(AppProps.I.getSessionStateUrl())
				.setDataUrlTemplate(AppProps.I.getFactDataUrlTemplate()) 
				.setStartDate(AppProps.I.getFactDataStartDate())
				.setEndDate(AppProps.I.getFactDataEndDate())
				.setLoadInterval(loadInterval)
				.setTableName(tableName)
				.setKeyColumnName(keyColumnName)
				.setIgnoreLastColumn(true)
				.setReportIds(reportId).build();
		File tempFile = new File(CommonUtils.generateFileName(tableName));
		Map<String, TypeAnalyzer> typeAnalyzers = new HashMap<>();
		proc.pullAndAnalyzeData(tempFile, typeAnalyzers);
	}
	
	private void process(String tableName, LoadMode loadMode, String... dataFileNames) throws ProcessorException {
		RedshiftDataProcessor proc = new RedshiftDataProcessor(tableName, loadMode, dataFileNames);
		proc.process();
	}
	
	private String renameFile(String tableName) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
		
		File oldFile = new File(CommonUtils.generateFileName(tableName));
		File newFile = new File(String.format(tableName + "_%s_%s.csv", dateFormat.format(AppProps.I.getFactDataStartDate()),
				dateFormat.format(AppProps.I.getFactDataEndDate())));
		oldFile.renameTo(newFile);		
		return newFile.getName();
	}
}
