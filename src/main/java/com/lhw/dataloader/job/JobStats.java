package com.lhw.dataloader.job;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class JobStats implements Serializable {
	public static enum JobState {SUCCEEDED, FAILED, RUNNING};
	
	private static final long serialVersionUID = 1L;

	private String name = null;
	private Date startTime = null;
	private Date endTime = null;
	private JobState state = JobState.RUNNING;
	private final List<String> messages = new ArrayList<>();
	private final List<JobStats> childJobStats = new ArrayList<>();
	
	public JobStats() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public JobState getState() {
		return state;
	}

	public void setState(JobState state) {
		this.state = state;
	}
	
	public List<String> getMessages() {
		return messages;
	}
	
	public void addMessage(String msg) {
		messages.add(msg);
	}
	
	public List<JobStats> getChildJobStats() {
		return childJobStats;
	}
	
	public void addChildJobStats(JobStats jobStats) {
		childJobStats.add(jobStats);
	}
	
	public boolean isSucceeded() {
		return getState() == JobState.SUCCEEDED
				&& !getChildJobStats().stream().filter(s -> !s.isSucceeded()).findAny().isPresent();
	}
	
	public String buildEmailBody() {
		StringBuffer sb = new StringBuffer();
		
		sb.append(getName()).append(System.lineSeparator());
		getMessages().forEach(m -> sb.append(m).append(System.lineSeparator()));
		getChildJobStats().forEach(c -> sb.append(c.buildEmailBody()).append(System.lineSeparator()));
		
		return sb.toString();
	}
}
