package com.lhw.dataloader.job.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.model.DataRow;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;

public class RestDataReader implements DataReader {
	private static final Logger logger = LoggerFactory.getLogger(RestDataReader.class.getName());

	private CloseableHttpClient httpClient = null;
	private CloseableHttpResponse response = null;
	private BufferedReader in = null;
	private CSVParser csvParser = null;
	private Iterator<CSVRecord> csvIterator = null;
	private boolean isClosed = false;

	private String sessionStateUrl = null;
	private String dataQueryUrlTemplate = null;
	private HeaderRow headerRow = null;
	private boolean ignoreLastColumn = false;
	
	public RestDataReader(String sessionStateUrl, String dataQueryUrlTemplate, boolean ignoreLastColumn) {
		this.sessionStateUrl = sessionStateUrl;
		this.dataQueryUrlTemplate = dataQueryUrlTemplate;
		this.ignoreLastColumn = ignoreLastColumn;
		
		httpClient = HttpClients.createDefault();
	}
	
	public RestDataReader(String sessionStateUrl, String dataQueryUrlTemplate) {
		this(sessionStateUrl, dataQueryUrlTemplate, false);
	}

	@Override
	public HeaderRow getHeaderRow() throws ReaderException {
		if (headerRow == null) {
			if (isClosed())
				throw new ReaderException("Reader is already closed.");
			
			headerRow = new HeaderRow();
		}
		else {
			return headerRow;
		}

		try {
			HttpGet httpGet = new HttpGet(sessionStateUrl);

			logger.debug("Executing request {}", httpGet.getRequestLine());

			// Create a custom response handler
			ResponseHandler<String> sessionStateResponseHandler = (response) -> {
				int status = response.getStatusLine().getStatusCode();
				if (status >= 200 && status < 300) {
					HttpEntity entity = response.getEntity();

					String jsonString = entity != null ? EntityUtils.toString(entity) : null;

					if (jsonString == null)
						return null;

					jsonString = jsonString.replace("'", "\"");
					logger.debug("Session state JSON = {}", jsonString);

					Map<String, String> metadata = new ObjectMapper().readValue(
							jsonString,
							new TypeReference<Map<String, String>>() {
					});
					String sessionState = metadata.get("sessionState");

					return sessionState;
				}
				else {
					throw new ClientProtocolException("Unexpected response status: "
							+ status);
				}
			};

			String sessionState = httpClient.execute(httpGet,
					sessionStateResponseHandler);
			logger.debug("Session State = {}", sessionState);

			if (sessionState == null) {
				throw new Exception("sessionState is empty.");
			}

			// Start query for data
			String url = dataQueryUrlTemplate.replace(CommonConstants.PARAM_SESSION_STATE, sessionState);
			logger.debug("Query URL: {}", url);

			httpGet = new HttpGet(url);

			in = null;
			response = null;
			csvParser = null;

			response = httpClient.execute(httpGet);
			int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode == HttpStatus.SC_OK) {
				in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), Charset.forName(
						AppProps.I.getCsvEncoding())));

				String line = null;
				boolean foundHeaderLine = false;

				while (!foundHeaderLine && (line = in.readLine()) != null) {
					if (!CommonUtils.isBlank(line)) {
						if (line.contains(AppProps.I.getCsvDelimiter())) {
							logger.debug("Found header line: " + line);
							foundHeaderLine = true;

							String[] headerNames = line.split(AppProps.I.getCsvDelimiterRegexPattern(), -1);
							int length = ignoreLastColumn ? headerNames.length - 1 : headerNames.length;
							for (int i = 0; i < length; i++) {
								String headerName = headerNames[i];
								headerRow.addHeaderName(headerName);
							}
						}
					}
				}
			}
			else {
				throw new Exception("Invalid HTTP status code " + statusCode);
			}
		}
		catch (Exception e) {
			throw new ReaderException(e.getMessage(), e);
		}

		return headerRow;
	}

	@Override
	public DataRow readDataRow() throws ReaderException {
		if (isClosed())
			throw new ReaderException("Reader is already closed.");
		
		try {
			if (csvParser == null) {
    			csvParser = new CSVParser(in, CSVFormat.newFormat(AppProps.I.getCsvDelimiter().charAt(0))
    					.withHeader(headerRow.getHeaderNames().toArray(new String[0]))
    					.withQuote(AppProps.I.getCsvQuote().charAt(0))
    					.withIgnoreEmptyLines(true).withAllowMissingColumnNames(ignoreLastColumn));
    			csvIterator = csvParser.iterator();
			}
			
			DataRow dataRow = new DataRow();

			if (csvIterator.hasNext()) {
				CSVRecord r = csvIterator.next();
				
				for (String headerName : headerRow.getHeaderNames()) {
					dataRow.addValue(headerName, r.get(headerName));
				}
			}
			else {
				dataRow = null;
			}
			
			return dataRow;
		}
		catch (Exception e) {
			throw new ReaderException(e.getMessage(), e);
		}
	}
	
	@Override
	public void close() {
		if (in != null) {
			try {
				in.close();
			}
			catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			
			in = null;
		}
		
		if (csvParser != null) {
			try {
				csvParser.close();
			}
			catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			
			csvParser = null;
		}
		
		if (response != null) {
			try {
				response.close();
			}
			catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			
			response = null;
		}
		
		if (httpClient != null) {
			try {
				httpClient.close();
			}
			catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
			
			httpClient = null;
		}
		
		isClosed = true;
	}
	
	public boolean isClosed() {
		return isClosed;
	}
}
