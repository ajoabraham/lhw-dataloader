package com.lhw.dataloader.job.reader;

import com.lhw.dataloader.model.DataRow;
import com.lhw.dataloader.model.HeaderRow;

public interface DataReader extends AutoCloseable {
	public HeaderRow getHeaderRow() throws ReaderException;
	public DataRow readDataRow() throws ReaderException;
	public boolean isClosed();
}
