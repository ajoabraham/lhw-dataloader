package com.lhw.dataloader.job.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.model.DataRow;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;

/**
 * This class is used to read local CSV file for processing
 * 
 * @author Tai Hu
 *
 */
public class CsvFileDataReader implements DataReader {
	private static final Logger logger = LoggerFactory.getLogger(
			CsvFileDataReader.class.getName());

	private BufferedReader in = null;
	private CSVParser csvParser = null;
	private Iterator<CSVRecord> csvIterator = null;
	private boolean isClosed = false;

	private File dataFile = null;
	private HeaderRow headerRow = null;
	
	private String csvDelimiter = AppProps.I.getCsvDelimiter();
	private String csvDelimiterRegex = AppProps.I.getCsvDelimiterRegexPattern();

	public CsvFileDataReader(File dataFile) {
		this.dataFile = dataFile;
	}

	@Override
	public HeaderRow getHeaderRow() throws ReaderException {
		if (headerRow == null) {
			if (isClosed())
				throw new ReaderException("Reader is already closed.");

			headerRow = new HeaderRow();
		}
		else {
			return headerRow;
		}

		try {
			// Reset csvParser
			csvParser = null;
			
			in = new BufferedReader(new InputStreamReader(new FileInputStream(dataFile), CommonConstants.DEFAULT_ENCODING));

			String line = null;
			boolean foundHeaderLine = false;

			while (!foundHeaderLine && (line = in.readLine()) != null) {
				if (!CommonUtils.isBlank(line)) {
//					if (line.contains(AppProps.I.getCsvDelimiter())) {
//					if (line.contains("\t")) {
					if (line.contains(csvDelimiter)) {
						logger.debug("Found header line: " + line);
						foundHeaderLine = true;

//						String[] headerNames = line.split(AppProps.I.getCsvDelimiterRegexPattern(), -1);
//						String[] headerNames = line.split("\t", -1);
						String[] headerNames = line.split(csvDelimiterRegex, -1);
						for (String headerName : headerNames) {
							headerRow.addHeaderName(headerName);
						}
					}
				}
			}
		}
		catch (Exception e) {
			throw new ReaderException(e.getMessage(), e);
		}

		return headerRow;
	}

	@Override
	public DataRow readDataRow() throws ReaderException {
		if (isClosed())
			throw new ReaderException("Reader is already closed.");

		try {
			if (csvParser == null) {
//				csvParser = new CSVParser(in, CSVFormat.newFormat(AppProps.I.getCsvDelimiter().charAt(0))
//				csvParser = new CSVParser(in, CSVFormat.newFormat('\t')
				csvParser = new CSVParser(in, CSVFormat.newFormat(csvDelimiter.charAt(0))
						.withHeader(headerRow.getHeaderNames().toArray(new String[0]))
						.withQuote(AppProps.I.getCsvQuote().charAt(0))
						.withIgnoreEmptyLines(true).withAllowMissingColumnNames(false));
				csvIterator = csvParser.iterator();
			}

			DataRow dataRow = new DataRow();

			if (csvIterator.hasNext()) {
				CSVRecord r = csvIterator.next();

				for (String headerName : headerRow.getHeaderNames()) {
					dataRow.addValue(headerName, r.get(headerName));
				}
			}
			else {
				dataRow = null;
			}

			return dataRow;
		}
		catch (Exception e) {
			throw new ReaderException(e.getMessage(), e);
		}
	}

	@Override
	public void close() {
		if (in != null) {
			try {
				in.close();
			}
			catch (IOException e) {
				logger.error(e.getMessage(), e);
			}

			in = null;
		}

		if (csvParser != null) {
			try {
				csvParser.close();
			}
			catch (IOException e) {
				logger.error(e.getMessage(), e);
			}

			csvParser = null;
		}

		isClosed = true;
	}

	public boolean isClosed() {
		return isClosed;
	}

	public String getCsvDelimiter() {
		return csvDelimiter;
	}

	public void setCsvDelimiter(String csvDelimiter) {
		this.csvDelimiter = csvDelimiter;
	}

	public String getCsvDelimiterRegex() {
		return csvDelimiterRegex;
	}

	public void setCsvDelimiterRegex(String csvDelimiterRegex) {
		this.csvDelimiterRegex = csvDelimiterRegex;
	}
}
