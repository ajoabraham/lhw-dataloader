package com.lhw.dataloader.job;
import static com.lhw.dataloader.common.JobConstants.*;

import java.io.File;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.job.JobStats.JobState;
import com.lhw.dataloader.job.processor.RedshiftDataProcessor;
import com.lhw.dataloader.util.CommonUtils;

public class ReloadBookingRawFactJob implements Job<JobStats> {
	private static final Logger logger = LoggerFactory.getLogger(ReloadBookingRawFactJob.class.getName());
	
	private File baseDir = null;
	private String dataFileName = null;
	
	public ReloadBookingRawFactJob(File baseDir) {
		this.baseDir = baseDir;
	}
	
	public ReloadBookingRawFactJob(String dataFileName) {
		this.dataFileName = dataFileName;
	}
	
	@Override
	public JobStats call() throws Exception {
		JobStats jobStats = new JobStats();
		jobStats.setName("Reload " + BOOKING_RAW_FACT_TABLE_NAME);
		jobStats.setStartTime(new Date());
		long startTime = System.currentTimeMillis();
				
		try {
			if (baseDir != null) {
    			File zipFile = CommonUtils.findLatestFile(baseDir, "NEWVeroBookingsExport");
    			logger.info("Unzip data file - " + zipFile.getAbsolutePath());
    			dataFileName = CommonUtils.unZipIt(zipFile);
			}
			
			RedshiftDataProcessor proc = new RedshiftDataProcessor(BOOKING_RAW_FACT_TABLE_NAME, LoadMode.REPLACE, dataFileName);
			proc.setCsvDelimiter("\t");
			proc.setCsvDelimiterRegex("\t");
			
			proc.process();
			CommonUtils.grantPermission();
    		String msg = "Successfully reloaded " + BOOKING_RAW_FACT_TABLE_NAME
    				+ " from " + dataFileName
    				+ " in " + (System.currentTimeMillis() - startTime) + "ms";
    		jobStats.addMessage(msg);
    		jobStats.setState(JobState.SUCCEEDED);
    		
    		logger.info(msg);
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			String msg = "Failed to reload " + BOOKING_RAW_FACT_TABLE_NAME + " - "
					+ e.getMessage();
			jobStats.addMessage(msg);
			jobStats.setState(JobState.FAILED);
		}
		finally {
			if (dataFileName != null) {
				File f = new File(dataFileName);
				f.delete();
				logger.info("Deleted data file - " + f.getAbsolutePath());
			}
		}
		
		return jobStats;
	}
}
