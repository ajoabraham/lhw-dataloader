package com.lhw.dataloader.common;

public enum LoadMode {
	REPLACE, APPEND, UPSERT;
}
