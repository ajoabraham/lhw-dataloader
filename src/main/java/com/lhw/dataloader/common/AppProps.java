package com.lhw.dataloader.common;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Date;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.util.CommonUtils;

/**
 * Load all properties from resources/conf/application.properties file
 * 
 * @author Tai Hu
 *
 */
public enum AppProps {
	I;
	
	private final Logger logger = LoggerFactory.getLogger(AppProps.class.getName());
	
	private Configuration configuration = null;
	
	private AppProps() {		
		try {
			configuration = new PropertiesConfiguration("conf/application.properties");
		}
		catch (ConfigurationException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public int getThreadPoolSize() {
		return configuration.getInt("com.lhw.dataloader.threadPoolSize", 10);
	}
	
	public String getSessionStateUrl() {
		return configuration.getString("com.lhw.dataloader.sessionStateUrl");
	}
	
	public String getDimDataUrlTemplate() {
		return configuration.getString("com.lhw.dataloader.dimDataUrlTemplate");
	}
	
	public String getCsvDelimiter() {
		return configuration.getString("com.lhw.dataloader.csvDelimiter");
	}
	
	public String getCsvDelimiterRegexPattern() {
		return configuration.getString("com.lhw.dataloader.csvDelimiterRegexPattern");
	}
	
	public String getCsvEncoding() {
		return configuration.getString("com.lhw.dataloader.csvEncoding", StandardCharsets.UTF_16LE.name());
	}
	
	public int getBatchInsertSize() {
		return configuration.getInt("com.lhw.dataloader.batchInsertSize", 3000);
	}
	
	public String getFactDataUrlTemplate() {
		return configuration.getString("com.lhw.dataloader.factDataUrlTemplate");
	}
	
	public Date getFactDataStartDate() throws ParseException {
		return DateUtils.parseDate(configuration.getString("com.lhw.dataloader.factDataStartDate"), CommonConstants.DEFAULT_DATE_FORMAT);
	}
	
	public Date getFactDataEndDate() throws ParseException {
		return DateUtils.parseDate(configuration.getString("com.lhw.dataloader.factDataEndDate"), CommonConstants.DEFAULT_DATE_FORMAT);
	}
	
	public int getDataLoadInterval() {
		return configuration.getInt("com.lhw.dataloader.dataLoadInterval", 10);
	}
	
	public long getRedshiftReadyDataFileSize() {
		return configuration.getLong("com.lhw.dataloader.redshiftReadyDataFileSize", 100000);
	}
	
	public int getFactRefreshInterval() {
		return configuration.getInt("com.lhw.dataloader.factRefreshInterval", 3);
	}
	
	public int getLcEnrollmentsRefreshInterval() {
		return configuration.getInt("com.lhw.dataloader.lcEnrollmentsRefreshInterval", 10);
	}
	
	public int getMaxRetry() {
		return configuration.getInt("com.lhw.dataloader.maxRetry");
	}
	
	public int getRetryInterval() {
		return configuration.getInt("com.lhw.dataloader.retryInterval");
	}
	
	public String getAccessKeyId() {
		String accessKeyId = System.getProperty("com.lhw.dataloader.accessKeyId");
		return CommonUtils.isBlank(accessKeyId) ? configuration.getString("com.lhw.dataloader.accessKeyId", "") : accessKeyId;
	}
	
	public String getAccessKey() {
		String accessKey = System.getProperty("com.lhw.dataloader.accessKey");
		return CommonUtils.isBlank(accessKey) ? configuration.getString("com.lhw.dataloader.accessKey", "") : accessKey;
	}
	
	public String getRegion() {
		return configuration.getString("com.lhw.dataloader.region", "");
	}
	
	public String getBucketName() {
		return configuration.getString("com.lhw.dataloader.bucketName", "");
	}
	
	public String getObjectKeyPrefix() {
		return configuration.getString("com.lhw.dataloader.objectKeyPrefix", "");
	}
	
	public String getCsvQuote() {
		return configuration.getString("com.lhw.dataloader.csvQuote", "\"");
	}
}
