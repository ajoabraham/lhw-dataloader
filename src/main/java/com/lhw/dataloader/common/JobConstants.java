package com.lhw.dataloader.common;

public interface JobConstants {
	// All dimension tables
	public static final String[] DIM_REPORT_IDS = {"12A57F7E46AFEB3A94A4228F067E29D5",
			"625B571543BD51488691A889CACDA2E2", "346D19D2404137CAEA10B69FEEF390A6",
			"B411798247C7D1CF31709D95989238DB", "82EAF52E43C5A570D2E852A42CF7AA14"};
	
	public static final String[] DIM_TABLE_NAMES = {"booking_channel_dim_base", "booking_source_code_dim_base",
			"property_dim_base", "booking_subchannel_codes_dim_base", "lc_source_code_dim_base"};
	
	// LC Enrollments table
	public static final String[] LC_ENROLLMENTS_REPORT_IDS = {"AE055A414BA792358F4E4AA9A1DFE1C0", "B206E9EF4541124C15AC22B7B6411104",
			"88BB28354FA9E42A3BD00B9C0E957B46", "F338811742043A8AA9A03CA9C3E036B2",
			"8A6EEA19478D3CF1D103218ECD99491F", "AD0909544126C0C14E775190D3F45554",
			"89832D424713F668FF0C6D95CC7EFA43"};
	public static final String LC_ENROLLMENTS_TABLE_NAME = "lc_enrollments_base";
	public static final String LC_ENROLLMENTS_KEY_COLUMN = "account_number"; 
	public static final String LC_ENROLLMENTS_DATE_COLUMN_1 = "lc_join_date_original";
	public static final String LC_ENROLLMENTS_DATE_COLUMN_2 = "lc_join_date_latest";
	public static final String LC_ENROLLMENTS_REFRESH_REPORT_ID = "915E85BC4CE3AA50663F1AB7EC603C29";
	
	// Booking mstr dim data table
	public static final String BOOKING_MSTR_DIM_REPORT_ID = "1401E6CC49C4FD5CE81211963524A46C";
	public static final String BOOKING_MSTR_DIM_TABLE_NAME = "booking_mstr_dim_base";
	public static final String BOOKING_MSTR_DIM_KEY_COLUMN = "booking_id";
	public static final String BOOKING_MSTR_DIM_DATE_COLUMN = "booking_date";
	public static final String BOOKING_MSTR_DIM_REFRESH_REPORT_ID = "30471FEC42BA773C127369822D05EC8C";
	
	// Booking status dim data table
	public static final String BOOKING_STATUS_DIM_REPORT_ID = "9DF55BDB48978DE27DFE9BB6B98DC526";
	public static final String BOOKING_STATUS_DIM_TABLE_NAME = "booking_status_dim_base";
	public static final String BOOKING_STATUS_DIM_KEY_COLUMN = "booking_id";
	public static final String BOOKING_STATUS_DIM_DATE_COLUMN = "booking_date";
	public static final String BOOKING_STATUS_DIM_REFRESH_REPORT_ID = "9DF55BDB48978DE27DFE9BB6B98DC526";
	
	// Lc member data table
	public static final String LC_MEMBER_TABLE_NAME = "lc_member_base";
	
	// Booking raw fact data table
	public static final String BOOKING_RAW_FACT_TABLE_NAME = "booking_raw_fact_base";
}
