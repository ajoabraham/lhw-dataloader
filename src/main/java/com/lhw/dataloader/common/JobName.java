package com.lhw.dataloader.common;

import com.lhw.dataloader.util.CommonUtils;

public enum JobName {
	LHW_DAILY_JOB("LhwDailyJob"),
	DIM_JOB("DimJob"), 
	REFRESH_BOOKING_MSTR_DIM_JOB("RefreshBookingMstrDimJob"),
	REFRESH_LC_ENROLLMENTS_JOB("RefreshLcEnrollmentsJob"),
	RELOAD_LC_MEMBER_JOB("ReloadLcMemberJob"),
	RELOAD_BOOKING_RAW_FACT_JOB("ReloadBookingRawFactJob"),
	REFRESH_BOOKING_RAW_FACT_JOB("RefreshBookingRawFactJob"),
	REFRESH_BOOKING_STATUS_DIM_JOB("RefreshBookingStatusDimJob");
	
	private String displayName = null;
	JobName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public static JobName toJobName(String displayName) {
		if (CommonUtils.isBlank(displayName)) return null;
		
		for (JobName jobName : JobName.values()) {
			if (displayName.equalsIgnoreCase(jobName.getDisplayName())) {
				return jobName;
			}
		}
		
		return null;
	}
}
