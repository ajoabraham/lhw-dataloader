package com.lhw.dataloader.common;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public interface CommonConstants {
	public static final long MAX_INT = 2147483647;
	public static final long MIN_INT = -2147483648;
	
	// Character encoding used for all files generated during the process.
	public static final String DEFAULT_ENCODING_NAME = StandardCharsets.UTF_8.name();
	public static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;
	
	// ISO 8601 date format (recommended by PostgreSQL)
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	// 	ISO 8601 timestamp format (recommended by PostgreSQL)
	public static final String DEFAULT_TIMESTAMP_FORMAT = DEFAULT_DATE_FORMAT + " HH:mm:ss";
	// Date format used by fact data URL
	public static final String FACT_URL_DATE_FORMAT = "MM/dd/yyyy";
	
	public static final String[] DATE_FORMATS = {"yyyy-MM-dd", "MM/dd/yyyy",
			"MM-dd-yyyy", "yyyy/MM/dd", "dd-MMM-yy"};
	public static final String[] TIMESTAMP_FORMATS = {"yyyy-MM-dd HH:mm:ss",
			"MM/dd/yyyy HH:mm:ss", "MM-dd-yyyy HH:mm:ss", "yyyy/MM/dd HH:mm:ss",
			"MM/dd/yyyy h:mm:ss a", "MM/dd/yyyy H:mm"};
	
	// All parameters replacement keys used in URL
	public static final String PARAM_SESSION_STATE = "[SESSION_STATE]";
	public static final String PARAM_REPORT_ID = "[REPORT_ID]";
	public static final String PARAM_INTERVAL = "[INTERVAL]";
	public static final String PARAM_DELIMITER = "[DELIMITER]";
	
	// If VARCHAR length is less than DEFAULT_VARCHAR_LENGTH_THRESHOLD, just return DEFAULT_VARCHAR_LENGTH
	public static final int DEFAULT_VARCHAR_LENGTH = 256;
	// If VARCHAR length is greater than DEFAULT_VARCHAR_LENGTH_THRESHOLD, add VARCHAR_LEGNTH_FACTOR% to the length
	public static final double VARCHAR_LENGTH_FACTOR = 0.2;
	public static final int DEFAULT_VARCHAR_LENGTH_THRESHOLD = DEFAULT_VARCHAR_LENGTH - (int)(DEFAULT_VARCHAR_LENGTH * VARCHAR_LENGTH_FACTOR);
}
