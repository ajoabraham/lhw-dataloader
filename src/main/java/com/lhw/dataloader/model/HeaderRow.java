package com.lhw.dataloader.model;

import java.util.ArrayList;
import java.util.List;

import com.lhw.dataloader.common.AppProps;

public final class HeaderRow {
	private List<String> headerNames = null;
	
	public HeaderRow() {
		headerNames = new ArrayList<>();
	}
	
	public List<String> getHeaderNames() {
		return headerNames;
	}
	
	public void addHeaderName(String name) {
		// FIXME Should fix this in Microstrategy
		if (name.startsWith(";"))
			name = name.substring(1, name.length());
		headerNames.add(name);
	}
	
	public int size() {
		return headerNames.size();
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < headerNames.size(); i++) {
			sb.append(headerNames.get(i));
			
			if (i != (headerNames.size() - 1)) {
				sb.append(AppProps.I.getCsvDelimiter());
			}
		}
		
		return sb.toString();
	}
}
