package com.lhw.dataloader.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lhw.dataloader.common.DataType;

public final class DataTypeInfo implements Cloneable {	
	private int columnPosition = 0;
	private String columnName = null;
	private DataType dataType = null;
	private int length = 0;
	private int precision = 0;
	private int scale = 0;
	private boolean isNullValue = false;
	
	public DataTypeInfo() {	
	}
	
	public DataTypeInfo(String columnName, int columnPosition) {
		this.columnName = columnName;
		this.columnPosition = columnPosition;
	}
	
	public int getColumnPosition() {
		return columnPosition;
	}

	public void setColumnPosition(int columnPosition) {
		this.columnPosition = columnPosition;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public DataType getDataType() {
		return dataType;
	}

	public void setDataType(DataType dataType) {
		this.dataType = dataType;
	}

	public int getLength() {		
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getPrecision() {
		return precision;
	}

	public void setPrecision(int precision) {
		this.precision = precision;
	}

	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}
	
	public String generateTypeSql() {
		StringBuffer sb = new StringBuffer();
		
		sb.append(dataType.getSqlTypeName());
		
		if (dataType == DataType.VARCHAR) {
			sb.append("(").append(getLength()).append(")");
		}
		else if (dataType == DataType.DECIMAL) {
			sb.append("(").append(getPrecision()).append(",")
				.append(getScale()).append(")");
		}
		
		return sb.toString();
	}
	
	@JsonIgnore
	public boolean isNullValue() {
		return isNullValue;
	}

	public void setIsNullValue(boolean isNullValue) {
		this.isNullValue = isNullValue;
	}

	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		
		if (obj == null || obj.getClass() != getClass()) {
			result = false;
		}
		else if (obj == this) {
			result = true;
		}
		else {
			DataTypeInfo dataTypeInfo = (DataTypeInfo) obj;
			
			if (getColumnPosition() == dataTypeInfo.getColumnPosition()
				&& getDataType() == dataTypeInfo.getDataType()
				&& getLength() == dataTypeInfo.getLength()
				&& ((getColumnName() != null && dataTypeInfo.getColumnName() != null && getColumnName().equals(dataTypeInfo.getColumnName())
					 || getColumnName() == null && dataTypeInfo.getColumnName() == null))) {
				if (getDataType() == DataType.DECIMAL
					&& (getPrecision() != dataTypeInfo.getPrecision() || getScale() != dataTypeInfo.getScale())) {
					result = false;
				}
				else {
					result = true;
				}
			}
		}
		
		return result;
	}
	
	@Override
	public int hashCode() {
		return getColumnPosition() + getDataType().hashCode() + getLength()
			+ (getColumnName() == null ? 0 : getColumnName().hashCode());
	}
	
	@Override
	public Object clone() {
		try {
			return super.clone();
		}
		catch (CloneNotSupportedException e) {
			// should never happen
		}
		
		return null;
	}
}
