package com.lhw.dataloader.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.PutObjectRequest;

public final class AmazonS3Service {
	private static final Logger logger = LoggerFactory.getLogger(AmazonS3Service.class.getName());
	
	private AmazonS3Client s3 = null;
	
	private AmazonS3Service(String accessKeyId, String accessKey) {
		s3 = new AmazonS3Client(new BasicAWSCredentials(accessKeyId, accessKey));
	}

	public static AmazonS3Service newInstance(String accessKeyId, String accessKey) {
		return new AmazonS3Service(accessKeyId, accessKey);
	}
	
	public List<String> getBucketNames() {
		List<String> bucketNames = new ArrayList<>();
		
		try {
			for (Bucket bucket : s3.listBuckets()) {
				bucketNames.add(bucket.getName());
			}
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		
		return bucketNames;
	}
	
	public static List<String> getRegionNames() {
		List<String> regionNames = new ArrayList<>();
		
		for (Regions region : Regions.values()) {
			regionNames.add(region.getName());
		}
		
		return regionNames;
	}
	
	public void uploadFile(String bucketName, String objectKey, File file) throws Exception {
		s3.putObject(new PutObjectRequest(bucketName, objectKey, file));
	}
	
	public void deleteObject(String bucketName, String objectKey) throws Exception {
		s3.deleteObject(bucketName, objectKey);
	}
}
