package com.lhw.dataloader.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public enum MailService {
	I;
	
	private static final Logger logger = LoggerFactory.getLogger(MailService.class.getName());
	private static final String SENDER_EMAIL = System.getProperty("sender_email", "tai.hu@lodr.io");
	private static final String RECIPIENT_EMAILS = System.getProperty("recipient_emails", "hutai66@gmail.com");
	private static final String EMAIL_PASSWORD = System.getenv("email_password") == null ? System.getProperty("email_password", "") : System.getenv("email_password");
	
	private MailService() {
	}

	public void sendMail(String subject, String msgBody) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.port", "465");
//		props.put("mail.smtp.ssl.enable", "true");
//		props.put("mail.smtp.ssl.trust", "*");

		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(SENDER_EMAIL, EMAIL_PASSWORD);
				}
			});
		
		session.setDebug(true);

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(SENDER_EMAIL));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(RECIPIENT_EMAILS));
			message.setSubject(subject);
			message.setText(msgBody);
			
			logger.info("Email Body: " + msgBody);
			
			Transport.send(message);
		} 
		catch (MessagingException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
