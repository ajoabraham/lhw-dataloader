package com.lhw.dataloader.util;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public enum ConnUtils {
	I;
	
	private HikariDataSource ds = null;
	
	private ConnUtils() {		
		HikariConfig config = new HikariConfig("/conf/connection.properties");
		ds = new HikariDataSource(config);
	}
	
	public Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
	
	public void shutdown() {
		if (ds != null) {
			ds.close();
		}
	}
}
