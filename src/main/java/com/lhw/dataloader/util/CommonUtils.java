package com.lhw.dataloader.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.common.DataType;
import com.lhw.dataloader.model.DataTypeInfo;
import com.lhw.dataloader.model.HeaderRow;

public final class CommonUtils {
	private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class.getName());
	private static String RESERVED_WORDS = "";
	private static String VALID_CHARS = "abcdefghijklmnopqrstuvwxyz0123456789_$";
	private static String INVALID_FIRST_CHARS = "0123456789$";
	private static String REPLACEMENT_CHAR = "_";

	static {
		try (Connection conn = ConnUtils.I.getConnection()) {
			RESERVED_WORDS = conn.getMetaData().getSQLKeywords();
			logger.debug("Database reserved words: " + RESERVED_WORDS);
		}
		catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

	private CommonUtils() {
	}

	public static String encodeUrl(String url) {
		try {
			return URLEncoder.encode(url, StandardCharsets.UTF_8.name());
		}
		catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage(), e);
		}

		return null;
	}

	public static boolean isBlank(String str) {
		return StringUtils.isBlank(str);
	}

	public static boolean isNumeric(String str) {
		if (!isBlank(str) && str.trim().length() > 1 && str.startsWith("0") && !str.contains(".")) {
			return false;
		}

		return NumberUtils.isParsable(str);
	}

	public static String normalizeColumnName(String name) {
		// Contain only ASCII letters, digits, underscore characters (_), or
		// dollar signs ($).
		// Begin with an alphabetic character or underscore character.
		// Subsequent characters may include letters, digits, underscores, or
		// dollar signs.
		// Be between 1 and 127 characters in length, not including quotes for
		// delimited identifiers.
		// Contain no quotation marks and no spaces.
		// Not be a reserved SQL key word
		StringBuffer sb = new StringBuffer();
		// To lowercase and remove all extra whitespaces.
		name = StringUtils.normalizeSpace(name.toLowerCase());
		for (char c : name.toCharArray()) {
			if (VALID_CHARS.contains(String.valueOf(c))) {
				if (sb.length() == 0 && INVALID_FIRST_CHARS.contains(String.valueOf(c))) {
					sb.append(REPLACEMENT_CHAR);
				}
				else {
					sb.append(c);
				}
			}
			else {
				sb.append(REPLACEMENT_CHAR);
			}
		}

		String normalizedName = sb.toString();
		// Remove leading and trailing underscore and multiple underscores
		normalizedName = StringUtils.replacePattern(normalizedName, "_{2,}", REPLACEMENT_CHAR);
		normalizedName = StringUtils.strip(normalizedName, REPLACEMENT_CHAR);

		if (RESERVED_WORDS.contains(normalizedName)) {
			normalizedName = REPLACEMENT_CHAR + normalizedName;
		}

		return normalizedName.length() > 127 ? normalizedName.substring(0, 127) : normalizedName;
	}

	public static String generateFileName(String prefix) {
		return prefix + ".csv";
	}

	public static boolean isDate(String input) {
		try {
			parseDate(input, CommonConstants.DATE_FORMATS);
			return true;
		}
		catch (ParseException e) {
			return false;
		}
	}

	public static boolean isTimestamp(String input) {
		try {
			parseDate(input, CommonConstants.TIMESTAMP_FORMATS);
			return true;
		}
		catch (ParseException e) {
			return false;
		}
	}

	public static java.sql.Date parseSqlDate(String input) {
		try {

			return new java.sql.Date(parseDate(input, CommonConstants.DATE_FORMATS).getTime());
		}
		catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}

		return null;
	}

	public static Timestamp parseSqlTimestamp(String input) {
		try {

			return new Timestamp(parseDate(input, CommonConstants.TIMESTAMP_FORMATS).getTime());
		}
		catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}

		return null;
	}

	public static String convertToDefaultDateFormat(String input) {
		try {
			Date date = parseDate(input, CommonConstants.DATE_FORMATS);
			input = new SimpleDateFormat(CommonConstants.DEFAULT_DATE_FORMAT).format(date);
		}
		catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}

		return input;
	}

	public static String convertToDefaultTimestampFormat(String input) {
		try {
			Date date = parseDate(input, CommonConstants.TIMESTAMP_FORMATS);
			input = new SimpleDateFormat(CommonConstants.DEFAULT_TIMESTAMP_FORMAT).format(date);
		}
		catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}

		return input;
	}

	public static String escape(String input) {
		return input.replace("'", "''");
	}

	// Return days between start and end (inclusive)
	public static long daysInBetween(Date start, Date end) {
		long diff = end.getTime() - start.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1;
	}

	public static Date copyDate(Date input) {
		try {
			return parseDate(new SimpleDateFormat(CommonConstants.DEFAULT_DATE_FORMAT).format(input),
					CommonConstants.DEFAULT_DATE_FORMAT);
		}
		catch (ParseException e) {
			logger.error(e.getMessage(), e);
		}

		return input;
	}

	public static String createInterval(Date start, Date end) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(CommonConstants.FACT_URL_DATE_FORMAT);
		return dateFormat.format(start) + "^" + dateFormat.format(end);
	}

	public static List<DataTypeInfo> toDataTypeInfos(HeaderRow headerRow, Map<String, TypeAnalyzer> typeAnalyzers) {
		List<DataTypeInfo> dataTypeInfos = new ArrayList<>();

		for (int i = 0; i < headerRow.getHeaderNames().size(); i++) {
			String headerName = headerRow.getHeaderNames().get(i);
			DataTypeInfo dataTypeInfo = typeAnalyzers.get(headerName).getDataTypeInfo();

			// TH 02/28/2016, if a column has all values empty (or null), the
			// type info will
			// be null as well. Just set it to varchar(256);
			if (dataTypeInfo == null) {
				dataTypeInfo = new DataTypeInfo(CommonUtils.normalizeColumnName(headerName), i);
				dataTypeInfo.setDataType(DataType.VARCHAR);
				dataTypeInfo.setLength(CommonConstants.DEFAULT_VARCHAR_LENGTH);
			}

			dataTypeInfos.add(dataTypeInfo);
		}

		return dataTypeInfos;
	}

	private static final String TABLE_EXISTS_QUERY = "select count(distinct tablename) from pg_table_def where "
			+ "schemaname = 'public' and tablename = '%s'";

	public static boolean doesTableExist(String tableName) {
		boolean tableExists = false;
		String query = String.format(TABLE_EXISTS_QUERY, tableName.toLowerCase());
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement();
				ResultSet resultSet = statement.executeQuery(query)) {
			if (resultSet.next()) {
				tableExists = resultSet.getInt(1) > 0;
			}
		}
		catch (Exception e) {
		}

		return tableExists;
	}

	public static String getStageTableName(String tableName) {
		return tableName + "_stage";
	}

	public static String getTempColumnName(String columnName) {
		return columnName + "_temp";
	}

	public static int getStringLength(String str) {
		return str.getBytes(CommonConstants.DEFAULT_ENCODING).length;
	}

	public static CSVPrinter createCSVPrinter(File file)
			throws FileNotFoundException, UnsupportedEncodingException, IOException {
		return createCSVPrinter(new PrintWriter(file, CommonConstants.DEFAULT_ENCODING_NAME));
	}

	public static CSVPrinter createCSVPrinter(Writer writer) throws IOException {
		return CSVFormat.newFormat(AppProps.I.getCsvDelimiter().charAt(0)).withQuote(
				AppProps.I.getCsvQuote().charAt(0)).withRecordSeparator(System.lineSeparator()).print(writer);
	}

	public static String escapeForCopyCommand(String input) {
		// Remove new line characters
		String output = input.replaceAll("\\r\\n|\\r|\\n", " ");
		if (output.trim().startsWith("'") || output.trim().endsWith("'")) {
			output = StringUtils.wrap(output, "\"");
		}

		return output;
	}

	private static Date parseDate(String input, String... parsePatterns) throws ParseException {
		try {
			return DateUtils.parseDateStrictly(input, parsePatterns);
		}
		catch (Exception e) {
			return DateUtils.parseDate(input, parsePatterns);
		}
	}

	public static File findLatestFile(File baseDir, String prefix) {
		if (!baseDir.isDirectory())
			return null;

		File latestFile = null;

		for (File f : baseDir.listFiles()) {
			if (f.getName().toLowerCase().startsWith(prefix.toLowerCase())) {
				if (latestFile == null || (latestFile.lastModified() < f.lastModified())) {
					latestFile = f;
				}
			}
		}

		return latestFile;
	}

	public static String unZipIt(File zipFile) {
		byte[] buffer = new byte[1024];
		String fileName = null;
		try {
			// get the zip file content
			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			if (ze != null) {
				fileName = ze.getName();
				FileOutputStream fos = new FileOutputStream(new File(fileName));

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
			}

			zis.closeEntry();
			zis.close();
		}
		catch (IOException e) {
			logger.error(e.getMessage(), e);
		}

		return fileName;
	}
	
	public static void grantPermission() {
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			statement.executeUpdate("grant insert on all tables in schema PUBLIC to lhwuser");
			statement.executeUpdate("grant update on all tables in schema PUBLIC to lhwuser");
			statement.executeUpdate("grant select on all tables in schema PUBLIC to lhwuser");
			statement.executeUpdate("grant delete on all tables in schema PUBLIC to lhwuser");
			
			logger.info("Successfully grant permission");
		}
		catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	private static final String CREATE_VIEWS = "create or replace view \"public\".\"lc_enrollments\" as" 
											 + "    select *"
											 + "    from ("
											 + "        select *, row_number() over(partition by consumer_id order by lc_join_date_latest desc) as rown"
											 + "        from \"public\".\"lc_enrollments_base\" t1 ) s"
											 + "    where rown=1";
	public static void recreateViews() {
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			statement.executeUpdate(CREATE_VIEWS);	
			
			logger.info("Successfully recreated views");
		}
		catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	private static final String DATA_TYPE_INFO_QUERY = "SELECT * FROM %s WHERE 1 = 2";
	// Load latest data type info from database
	public static List<DataTypeInfo> getPreviousDataTypeInfos(String tableName) {
		List<DataTypeInfo> dataTypeInfos = new ArrayList<>();
		try (Connection conn = ConnUtils.I.getConnection();
				PreparedStatement statement = conn.prepareStatement(String.format(DATA_TYPE_INFO_QUERY, tableName))) {
			ResultSet resultSet = statement.executeQuery();
			ResultSetMetaData metaData = resultSet.getMetaData();
			
			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				DataTypeInfo dataTypeInfo = new DataTypeInfo();
				dataTypeInfo.setColumnPosition(i - 1);
				dataTypeInfo.setColumnName(metaData.getColumnName(i));
				dataTypeInfo.setLength(metaData.getColumnDisplaySize(i));
				DataType dataType = DataType.toDataType(metaData.getColumnType(i));
				if (dataType == DataType.DECIMAL) {
					dataTypeInfo.setPrecision(metaData.getPrecision(i));
					dataTypeInfo.setScale(metaData.getScale(i));
				}
				dataTypeInfo.setDataType(dataType);
				
				dataTypeInfos.add(dataTypeInfo);
			}
		}
		catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}

		return dataTypeInfos;
	}
	
	private static final String REMOVE_DUPLICATED_ROWS_QUERY = "CREATE TABLE %s AS SELECT * FROM (SELECT *, pg_catalog.row_number() OVER (PARTITION BY t1.%s ORDER BY t1.%s DESC) AS rown FROM %s t1) t WHERE t.rown = 1";
	public static void removeDuplicatedRows(String tableName, String partitionBy, String orderBy) {
		String tempTableName = tableName + "_temp";
		String removeDuplicatedRowsQuery = String.format(REMOVE_DUPLICATED_ROWS_QUERY, tableName, partitionBy, orderBy, tempTableName);
		
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			statement.execute("alter table " + tableName + " rename to " + tempTableName);
			statement.execute(removeDuplicatedRowsQuery);
			// Drop temp table
			statement.execute("drop table if exists " + tempTableName);
			// Drop rown column
			statement.execute("alter table " + tableName + " drop column rown");
		}
		catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}
	
	private static final String DROP_VIEWS_QUERY = "drop view \"public\".\"lc_enrollments\"";
	public static void dropViews() throws Exception {
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			statement.executeUpdate(DROP_VIEWS_QUERY);
			
			logger.info("Successfully drop views");
		}
	}
}
