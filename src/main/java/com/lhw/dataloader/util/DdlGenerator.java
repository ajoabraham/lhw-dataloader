package com.lhw.dataloader.util;

import java.util.List;

import com.lhw.dataloader.model.DataTypeInfo;

public final class DdlGenerator {

	private DdlGenerator() {
	}
	
	public static String generateDdl(String tableName, List<DataTypeInfo> dataTypeInfos) {
		StringBuffer sb = new StringBuffer();
		
		sb.append("create table ").append(tableName).append(" (").append(System.lineSeparator());
		
		for (int i  = 0; i < dataTypeInfos.size(); i++) {
			DataTypeInfo dataTypeInfo = dataTypeInfos.get(i);
			sb.append(dataTypeInfo.getColumnName()).append("    ").append(dataTypeInfo.generateTypeSql());
			
			if (i != (dataTypeInfos.size() - 1)) {
				sb.append(",").append(System.lineSeparator());
			}
		}
		
		sb.append(")");
		
		return sb.toString();
	}
}
