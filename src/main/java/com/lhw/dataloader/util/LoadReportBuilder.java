package com.lhw.dataloader.util;

/**
 * Build report for each fact data refresh and email it to system admin. It should contains
 * all stats and errors during data refresh.
 * 
 * @author Tai Hu
 *
 */
public enum LoadReportBuilder {
	I;
	
	private StringBuffer content = new StringBuffer();
	
	private LoadReportBuilder() {	
	}
	
	public synchronized void addContent(String msg) {
		content.append(msg).append(System.lineSeparator());
	}
}
