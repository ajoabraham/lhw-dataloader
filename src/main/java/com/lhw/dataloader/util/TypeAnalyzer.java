package com.lhw.dataloader.util;

import org.apache.commons.lang3.math.NumberUtils;

import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.common.DataType;
import com.lhw.dataloader.model.DataTypeInfo;

public final class TypeAnalyzer {
	// Record currently identified type information
	private DataTypeInfo dataTypeInfo = null;
//	private AnalyticStats stats = null;
	private String columnName = null;
	private int columnPosition = 0;
	
	public TypeAnalyzer(String columnName, int columnPosition) {
		this.columnName = columnName;
		this.columnPosition = columnPosition;
//		stats = new AnalyticStats();
	}

	public void analyze(String value) {
		// Determine given value's type
		if (CommonUtils.isBlank(value)) {
//			stats.incrementIgnoreCount();
			return;
		}
		
		// This is the first time,
		if (dataTypeInfo == null) {
			dataTypeInfo = determineDataTypeInfo(value);
		}
		else {
			updateDataTypeInfo(dataTypeInfo, value);
		}
	}
	
	public DataTypeInfo getDataTypeInfo() {
		if (dataTypeInfo == null) {
			DataTypeInfo defaultType = new DataTypeInfo();
			defaultType.setColumnName(columnName);
			defaultType.setColumnPosition(columnPosition);
			defaultType.setDataType(DataType.VARCHAR);
			defaultType.setLength(CommonConstants.DEFAULT_VARCHAR_LENGTH);
			defaultType.setIsNullValue(true);
			
			return defaultType;
		}
		else {
			return dataTypeInfo;
		}
	}
	
	public void printStats() {
		System.out.println("Final Data Type : " + dataTypeInfo.getDataType());
		System.out.println("Length: " + dataTypeInfo.getLength());
		if (dataTypeInfo.getDataType() == DataType.DECIMAL) {
			System.out.println("Precision: " + dataTypeInfo.getPrecision() + " Scale: " + dataTypeInfo.getScale());
		}
		System.out.println("==================================================");
		
//		System.out.println(stats);
	}
	
	// Determine the data type based on given String value.
	private DataTypeInfo determineDataTypeInfo(String value) {
		DataTypeInfo typeInfo = new DataTypeInfo(columnName, columnPosition);
		typeInfo.setLength(CommonUtils.getStringLength(value));
		
		if (CommonUtils.isNumeric(value)) {
			if (value.contains(".")) {
				typeInfo.setDataType(DataType.DECIMAL);
				String[] parts = value.split("\\.");
				typeInfo.setPrecision(parts[0].length() + parts[1].length());
				typeInfo.setScale(parts[1].length());
//				stats.incrementDecimalCount();
			}
			else {
				try {
    				long v = Long.parseLong(value);
    				
    				if (v > CommonConstants.MAX_INT || v < CommonConstants.MIN_INT) {
    					typeInfo.setDataType(DataType.BIGINT);
//    					stats.incrementBigIntCount();
    				}
    				else {
    					typeInfo.setDataType(DataType.INTEGER);
//    					stats.incrementIntCount();
    				}
				}
				catch (NumberFormatException e) {
					// Number is too big to parse.
					typeInfo.setDataType(DataType.BIGINT);
//					stats.incrementBigIntCount();
				}
			}
		}
		else if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
			typeInfo.setDataType(DataType.BOOLEAN);
//			stats.incrementBooleanCount();
		}
		else if (CommonUtils.isDate(value)) {
			typeInfo.setDataType(DataType.DATE);
//			stats.incrementDateCount();
		}
		else if (CommonUtils.isTimestamp(value)) {
			typeInfo.setDataType(DataType.TIMESTAMP);
//			stats.incrementTimestampCount();
		}
		else {
			typeInfo.setDataType(DataType.VARCHAR);
//			stats.incrementVarcharCount();
		}
			
		adjustVarcharSize(typeInfo);
		
		return typeInfo;
	}
	
	// Update given dataTypeInfo based on current value
	private void updateDataTypeInfo(DataTypeInfo typeInfo, String value) {
		if (CommonUtils.isBlank(value)) {
			return;
		}

		int previousLength = typeInfo.getLength();
		
		int length = CommonUtils.getStringLength(value);
		if (length > typeInfo.getLength()) {
			typeInfo.setLength(length);
		}

		if (typeInfo.getDataType() == DataType.VARCHAR) {
			// do nothing
		}
		else if (typeInfo.getDataType() == DataType.INTEGER) {
			if (CommonUtils.isNumeric(value)) {
				if (value.contains(".")) {
					typeInfo.setDataType(DataType.DECIMAL);
					String[] parts = value.split("\\.");
					typeInfo.setPrecision(parts[0].length() + parts[1].length());
					typeInfo.setScale(parts[1].length());
				}
				else {
					try {
	    				long v = Long.parseLong(value);
	    				
	    				if (v > CommonConstants.MAX_INT || v < CommonConstants.MIN_INT) {
	    					typeInfo.setDataType(DataType.BIGINT);
	    				}
					}
					catch (NumberFormatException e) {
						// Number is too big to parse.
						typeInfo.setDataType(DataType.BIGINT);
					}
				}
			}
			else {
				dataTypeInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.BIGINT) {
			if (CommonUtils.isNumeric(value)) {
				if (value.contains(".")) {
					typeInfo.setDataType(DataType.DECIMAL);
					String[] parts = value.split("\\.");
					typeInfo.setPrecision(parts[0].length() + parts[1].length());
					typeInfo.setScale(parts[1].length());
				}
			}
			else {
				dataTypeInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.DECIMAL) {			
			if (CommonUtils.isNumeric(value)) {
				if (value.contains(".")) {
					typeInfo.setDataType(DataType.DECIMAL);
					String[] parts = value.split("\\.");
					int leftSize = NumberUtils.max(parts[0].length(), typeInfo.getPrecision() - typeInfo.getScale());
					int scale = NumberUtils.max(parts[1].length(), typeInfo.getScale());
					typeInfo.setPrecision(leftSize + scale);
					typeInfo.setScale(scale);
				}
				else {
					int leftSize = NumberUtils.max(value.length(), typeInfo.getPrecision() - typeInfo.getScale());
					typeInfo.setPrecision(leftSize + typeInfo.getScale());
				}
			}
			else {				
				dataTypeInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.BOOLEAN) {
			if (!value.equalsIgnoreCase("true") && !value.equalsIgnoreCase("false")) {
				typeInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.DATE) {
			if (!CommonUtils.isDate(value)) {
				typeInfo.setDataType(DataType.VARCHAR);
			}
		}
		else if (typeInfo.getDataType() == DataType.TIMESTAMP) {
			if (!CommonUtils.isTimestamp(value)) {
				typeInfo.setDataType(DataType.VARCHAR);
			}
		}
		
		if (previousLength < typeInfo.getLength() && typeInfo.getDataType() == DataType.VARCHAR) {
			adjustVarcharSize(typeInfo);
		}
	}
	
	// This method should always return a new DataTypeInfo object.
	public static DataTypeInfo mergeDataTypeInfo(DataTypeInfo t1, DataTypeInfo t2) {
		DataTypeInfo merged = (DataTypeInfo) t1.clone();
				
		if (t2.isNullValue()) {
			// do nothing
		}
		else if (merged.getDataType() == DataType.VARCHAR) {
			// If current data type is anything other than varchar, then just ignore it.
			// Otherwise, check the length of current data type and record the longer one
			if (t2.getDataType() == DataType.VARCHAR 
					&& t2.getLength() > merged.getLength()) {
				merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.INTEGER) {
			if (t2.getDataType() == DataType.INTEGER) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
			else if (t2.getDataType() == DataType.BIGINT
						|| t2.getDataType() == DataType.DECIMAL) {
				merged = (DataTypeInfo) t2.clone();
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.BIGINT) {
			if (t2.getDataType() == DataType.INTEGER) {
				// Ignore
			}
			else if (t2.getDataType() == DataType.BIGINT) {
				if (t2.getLength() > merged.getLength()) {
					merged.setLength(t2.getLength());
				}
			}
			else if (t2.getDataType() == DataType.DECIMAL) {
				if (merged.getLength() > (t2.getPrecision() - t2.getScale()))
					t2.setPrecision(merged.getLength() + t2.getScale());
				
				merged = (DataTypeInfo) t2.clone();
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.DECIMAL) {
			if (t2.getDataType() == DataType.INTEGER
					|| t2.getDataType() == DataType.BIGINT) {
				if (t2.getLength() > (merged.getPrecision() - merged.getScale()))
					merged.setPrecision(t2.getLength() + merged.getScale());
			}
			else if (t2.getDataType() == DataType.DECIMAL) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
				
				int scale = merged.getScale() > t2.getScale() ? merged.getScale() : t2.getScale();
				int leftSize = (merged.getPrecision() - merged.getScale()) > (t2.getPrecision() - t2.getScale()) ?
						(merged.getPrecision() - merged.getScale()) : (t2.getPrecision() - t2.getScale());
				merged.setScale(scale);
				merged.setPrecision(leftSize + scale);
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.BOOLEAN) {
			if (t2.getDataType() == DataType.BOOLEAN) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.DATE) {
			if (t2.getDataType() == DataType.DATE) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
		else if (merged.getDataType() == DataType.TIMESTAMP) {
			if (t2.getDataType() == DataType.TIMESTAMP) {
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
			else {
				merged.setDataType(DataType.VARCHAR);
				if (t2.getLength() > merged.getLength())
					merged.setLength(t2.getLength());
			}
		}
				
		return merged;
	}
	
	private void adjustVarcharSize(DataTypeInfo typeInfo) {
		if (typeInfo.getDataType() == DataType.VARCHAR) {
			int length = typeInfo.getLength();
			if (length <= CommonConstants.DEFAULT_VARCHAR_LENGTH_THRESHOLD) {
				typeInfo.setLength(CommonConstants.DEFAULT_VARCHAR_LENGTH);
			}
			else {
				typeInfo.setLength(length + (int)(length * CommonConstants.VARCHAR_LENGTH_FACTOR));
			}
		}
	}
	
//	private class AnalyticStats {
//		private long totalProcessed = 0;
//		private long intCount = 0;
//		private long bigIntCount = 0;
//		private long decimalCount = 0;
//		private long booleanCount = 0;
//		private long varcharCount = 0;
//		private long dateCount = 0;
//		private long timestampCount = 0;
//		private long ignoreCount = 0;
//		
//		public AnalyticStats() {
//			
//		}
//
//		public long getTotalProcessed() {
//			return totalProcessed;
//		}
//
//		private void incrementTotalProcessed() {
//			this.totalProcessed++;
//		}
//
//		public long getIntCount() {
//			return intCount;
//		}
//
//		public void incrementIntCount() {
//			incrementTotalProcessed();
//			this.intCount++;
//		}
//
//		public long getBigIntCount() {
//			return bigIntCount;
//		}
//
//		public void incrementBigIntCount() {
//			incrementTotalProcessed();
//			this.bigIntCount++;
//		}
//
//		public long getDecimalCount() {
//			return decimalCount;
//		}
//
//		public void incrementDecimalCount() {
//			incrementTotalProcessed();
//			this.decimalCount++;
//		}
//
//		public long getBooleanCount() {
//			return booleanCount;
//		}
//
//		public void incrementBooleanCount() {
//			incrementTotalProcessed();
//			this.booleanCount++;
//		}
//
//		public long getVarcharCount() {
//			return varcharCount;
//		}
//
//		public void incrementVarcharCount() {
//			incrementTotalProcessed();
//			this.varcharCount++;
//		}
//
//		public long getDateCount() {
//			return dateCount;
//		}
//
//		public void incrementDateCount() {
//			incrementTotalProcessed();
//			this.dateCount++;
//		}
//
//		public long getTimestampCount() {
//			return timestampCount;
//		}
//
//		public void incrementTimestampCount() {
//			incrementTotalProcessed();
//			this.timestampCount++;
//		}
//
//		public long getIgnoreCount() {
//			return ignoreCount;
//		}
//
//		public void incrementIgnoreCount() {
//			incrementTotalProcessed();
//			this.ignoreCount++;
//		}
//		
//		@Override
//		public String toString() {
//			StringBuffer sb = new StringBuffer();
//			
//			sb.append("Total number of values analyzed: ").append(getTotalProcessed()).append(System.lineSeparator());
//			sb.append("Total number of integer type: ").append(getIntCount()).append(System.lineSeparator());
//			sb.append("Total number of bigint type: ").append(getBigIntCount()).append(System.lineSeparator());
//			sb.append("Total number of decimal type: ").append(getDecimalCount()).append(System.lineSeparator());
//			sb.append("Total number of boolean type: ").append(getBooleanCount()).append(System.lineSeparator());
//			sb.append("Total number of varchar type: ").append(getVarcharCount()).append(System.lineSeparator());
//			sb.append("Total number of date type: ").append(getDateCount()).append(System.lineSeparator());
//			sb.append("Total number of timestamp type: ").append(getTimestampCount()).append(System.lineSeparator());
//			sb.append("Total number of ignored string: ").append(getIgnoreCount()).append(System.lineSeparator());
//			
//			return sb.toString();
//		}
//	}
}
