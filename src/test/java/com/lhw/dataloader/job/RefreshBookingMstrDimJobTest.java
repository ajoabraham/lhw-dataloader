package com.lhw.dataloader.job;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_DATE_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_REFRESH_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_TABLE_NAME;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RefreshBookingMstrDimJobTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRefreshFactDataJob() throws Exception {
		// TH 02/01/2016, this test case will load all data from last load minus refresh interval until yesterday.
//		RefreshFactDataJob job = new RefreshFactDataJob("booking_fact1", "booking_id", "booking_date", "1401E6CC49C4FD5CE81211963524A46C");
		RefreshBookingMstrDimJob job = new RefreshBookingMstrDimJob(BOOKING_MSTR_DIM_TABLE_NAME, BOOKING_MSTR_DIM_KEY_COLUMN, 
				BOOKING_MSTR_DIM_DATE_COLUMN, BOOKING_MSTR_DIM_REFRESH_REPORT_ID);
		long startTime = System.currentTimeMillis();
		job.call();
		System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to refresh data.");
	}
}
