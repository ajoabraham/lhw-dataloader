package com.lhw.dataloader.job.processor;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lhw.dataloader.common.JobConstants;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

public class LocalDataProcessorTest {
	private String[] dataFileNames = {"booking_fact_01012009_12312009.csv",
			"booking_fact_01012010_12312010.csv", "booking_fact_01012011_12312011.csv",
			"booking_fact_01012012_12312012.csv", "booking_fact_01012013_12312013.csv",
			"booking_fact_01012014_12312014.csv", "booking_fact_01012015_12312015.csv",
			"booking_fact_01012016_02012016.csv"
	};
	
	private String tableName = JobConstants.BOOKING_MSTR_DIM_TABLE_NAME;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPullAndAnalyzeData() throws Exception {		
		long startTime = System.currentTimeMillis();
		LocalDataProcessor proc = new LocalDataProcessor(tableName, dataFileNames);
		File tempFile = new File(CommonUtils.generateFileName(tableName));
		Map<String, TypeAnalyzer> typeAnalyzers = new HashMap<>();
		HeaderRow headerRow = proc.pullAndAnalyzeData(tempFile, typeAnalyzers);
		assertTrue(headerRow.getHeaderNames().size() > 0);
		assertTrue(tempFile.exists());
		System.out.println("Took " + (System.currentTimeMillis() - startTime)
				+ "ms to pull and analyze " + tableName);
	}
	
	@Test
	public void testProcess() throws Exception {
		long startTime = System.currentTimeMillis();
		LocalDataProcessor proc = new LocalDataProcessor(tableName, dataFileNames);
		proc.process();
		System.out.println("Took " + (System.currentTimeMillis() - startTime)
				+ "ms to process " + tableName);
	}
}
