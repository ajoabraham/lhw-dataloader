package com.lhw.dataloader.job.processor;

import static com.lhw.dataloader.common.JobConstants.DIM_REPORT_IDS;
import static com.lhw.dataloader.common.JobConstants.DIM_TABLE_NAMES;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_REPORT_IDS;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_TABLE_NAME;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

public class DimDataProcessorTest {	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testPullAndAnalyzeData() throws Exception {		
		for (int i = 0; i < DIM_REPORT_IDS.length; i++) {
			long startTime = System.currentTimeMillis();
			pullAndAnalyzeData(DIM_TABLE_NAMES[i], DIM_REPORT_IDS[i]);
			System.out.println("Took " + (System.currentTimeMillis() - startTime)
					+ "ms to pull and analyze " + DIM_TABLE_NAMES[i]);
		}
	}

	@Test
	public void testPullAndAnalyzeDataForMultipleReportIds() throws Exception {
		long startTime = System.currentTimeMillis();
		pullAndAnalyzeData(LC_ENROLLMENTS_TABLE_NAME, LC_ENROLLMENTS_REPORT_IDS);
		System.out.println("Took " + (System.currentTimeMillis() - startTime)
				+ "ms to pull and analyze " + LC_ENROLLMENTS_TABLE_NAME);
	}
	
	private void pullAndAnalyzeData(String tableName, String... reportIds) throws Exception {
		DimDataProcessor proc = new DimDataProcessor(AppProps.I.getSessionStateUrl(), 
				AppProps.I.getDimDataUrlTemplate(), tableName, reportIds);
		File tempFile = new File(CommonUtils.generateFileName(tableName));
		Map<String, TypeAnalyzer> typeAnalyzers = new HashMap<>();
		HeaderRow headerRow = proc.pullAndAnalyzeData(tempFile, typeAnalyzers);
		assertTrue(headerRow.getHeaderNames().size() > 0);
		assertTrue(tempFile.exists());
	}
	
	// TH 05/02/2016, use this method to reload all dimension tables.
	// No need to manually drop all dimension tables in target database.
	@Test
	public void testInitLoadDimDataProcess() throws ProcessorException {		
		for (int i = 0; i < DIM_REPORT_IDS.length; i++) {
			long startTime = System.currentTimeMillis();
			DimDataProcessor proc = new DimDataProcessor(AppProps.I.getSessionStateUrl(), 
					AppProps.I.getDimDataUrlTemplate(), DIM_TABLE_NAMES[i], DIM_REPORT_IDS[i]);
			proc.process();
			System.out.println("Took " + (System.currentTimeMillis() - startTime)
					+ "ms to process " + DIM_TABLE_NAMES[i]);
		}
	}
	
	// TH 05/02/2016, use this method to reload lc_enrollments table.
	@Test
	public void testInitLoadLcEnrollmentsDataProcess() throws ProcessorException {
		long startTime = System.currentTimeMillis();
		DimDataProcessor proc = new DimDataProcessor(AppProps.I.getSessionStateUrl(), 
				AppProps.I.getDimDataUrlTemplate(), LC_ENROLLMENTS_TABLE_NAME, LC_ENROLLMENTS_REPORT_IDS);
		proc.process();
		System.out.println("Took " + (System.currentTimeMillis() - startTime)
				+ "ms to process " + LC_ENROLLMENTS_TABLE_NAME);
	}
}
