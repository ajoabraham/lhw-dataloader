package com.lhw.dataloader.job.processor;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.BOOKING_RAW_FACT_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.LC_MEMBER_TABLE_NAME;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lhw.dataloader.common.LoadMode;

public class RedshiftDataProcessorTest {
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	// Use this method to load local CSV files into booking_mstr_dim_base table in Redshift. 
	// Use LoadDataProcessorTest.testPullBookignMstrDimData() to pull data from Microstrategy server.
	@Test
	public void testLoadBookingMstrDimTable() throws Exception {
//		String[] dataFileNames = {"booking_fact_01012009_02032016.csv", "booking_fact_02042016_02242016.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_06152016.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_11252016.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_11282016.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_01042017.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_02142017.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_03192017.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_04102017.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_05042017.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_06192017.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_07082017.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_08052017.csv"};
		String[] dataFileNames = {"booking_mstr_dim_base_01012010_09082017.csv"};
		process(BOOKING_MSTR_DIM_TABLE_NAME, LoadMode.REPLACE, dataFileNames);
	}
	
	// Use this method to load local CSV files into booking_status_dim_base table in Redshift. 
	// Use LoadDataProcessorTest.testPullBookignStatusDimData() to pull data from Microstrategy server.
	@Test
	public void testLoadBookingStatusDimTable() throws Exception {
//		String[] dataFileNames = {"booking_fact_01012009_02032016.csv", "booking_fact_02042016_02242016.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_06152016.csv"};
//		String[] dataFileNames = {"booking_mstr_dim_base_01012010_11252016.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_11292016.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_01042017.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_02142017.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_03192017.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_04102017.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_05042017.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_06192017.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_07082017.csv"};
//		String[] dataFileNames = {"booking_status_dim_base_01012010_08052017.csv"};
		String[] dataFileNames = {"booking_status_dim_base_01012010_09082017.csv"};
		process(BOOKING_STATUS_DIM_TABLE_NAME, LoadMode.REPLACE, dataFileNames);
	}
	
	@Test
	public void testProcessLcEnrollmentsData() throws ProcessorException {
		process(LC_ENROLLMENTS_TABLE_NAME, LoadMode.REPLACE, new String[] {"vero_lc_enrollments_raw.csv"});
	}
	
	@Test
	public void testProcessLcMemberData() throws ProcessorException {
		process(LC_MEMBER_TABLE_NAME, LoadMode.REPLACE, "LCMemberExport_20160713.txt");
	}
	
	@Test
	public void testProcessBookingRawFactData() throws ProcessorException {
		process(BOOKING_RAW_FACT_TABLE_NAME, LoadMode.REPLACE, "2016.07.13.14.20.12.txt");
	}
	
	private void process(String tableName, LoadMode loadMode, String... dataFileNames) throws ProcessorException {
		long startTime = System.currentTimeMillis();
		RedshiftDataProcessor proc = new RedshiftDataProcessor(tableName, loadMode, dataFileNames);
		proc.process();
		System.out.println("Took " + (System.currentTimeMillis() - startTime)
				+ "ms to process " + tableName);
	}
}
