package com.lhw.dataloader.job.processor;

import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_MSTR_DIM_TABLE_NAME;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.BOOKING_STATUS_DIM_TABLE_NAME;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.LoadMode;
import com.lhw.dataloader.job.processor.LoadDataProcessor.LoadDataProcessorBuilder;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;
import com.lhw.dataloader.util.TypeAnalyzer;

public class LoadDataProcessorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	// Use this method to pull raw data from Microstrategy server and store it into local file for
	// booking_mstr_dim_base table.
	@Test
	public void testPullBookingMstrDimData() throws Exception {
		long startTime = System.currentTimeMillis();
		pullAndAnalyzeData(BOOKING_MSTR_DIM_TABLE_NAME, BOOKING_MSTR_DIM_KEY_COLUMN, BOOKING_MSTR_DIM_REPORT_ID);
		System.out.println("Took " + (System.currentTimeMillis() - startTime)
				+ "ms to pull and analyze " + BOOKING_MSTR_DIM_TABLE_NAME);
	}
	
	// Use this method to pull raw data from Microstrategy server and store it into local file for
	// booking_status_dim_base table. Recommended to load one year at a time
	@Test
	public void testPullBookingStatusDimData() throws Exception {
		long startTime = System.currentTimeMillis();
		pullAndAnalyzeData(BOOKING_STATUS_DIM_TABLE_NAME, BOOKING_STATUS_DIM_KEY_COLUMN, BOOKING_STATUS_DIM_REPORT_ID);
		System.out.println("Took " + (System.currentTimeMillis() - startTime)
				+ "ms to pull and analyze " + BOOKING_STATUS_DIM_TABLE_NAME);
	}

	private void pullAndAnalyzeData(String tableName, String keyColumnName, String reportId) throws Exception {
		LoadDataProcessor proc = LoadDataProcessorBuilder.create().setSessionStateUrl(AppProps.I.getSessionStateUrl())
				.setDataUrlTemplate(AppProps.I.getFactDataUrlTemplate()) 
				.setStartDate(AppProps.I.getFactDataStartDate())
				.setEndDate(AppProps.I.getFactDataEndDate())
				.setLoadInterval(AppProps.I.getDataLoadInterval())
				.setTableName(tableName)
				.setKeyColumnName(keyColumnName)
				.setIgnoreLastColumn(true)
				.setReportIds(reportId).build();
		File tempFile = new File(CommonUtils.generateFileName(tableName));
		Map<String, TypeAnalyzer> typeAnalyzers = new HashMap<>();
		HeaderRow headerRow = proc.pullAndAnalyzeData(tempFile, typeAnalyzers);
		assertTrue(headerRow.getHeaderNames().size() > 0);
		assertTrue(tempFile.exists());
	}

	// Using this method to reload fact data table. NOTES, this method is using DB insert, so if data is huge, user
	// should use RedshiftReadyDataProcessor instead of this one.
	@Test
	public void testInitLoadFactDataProcess() throws ParseException, ProcessorException {		
		LoadDataProcessor proc = LoadDataProcessorBuilder.create().setSessionStateUrl(AppProps.I.getSessionStateUrl())
				.setDataUrlTemplate(AppProps.I.getFactDataUrlTemplate()) 
				.setStartDate(AppProps.I.getFactDataStartDate())
				.setEndDate(AppProps.I.getFactDataEndDate())
				.setLoadInterval(AppProps.I.getDataLoadInterval())
				.setTableName(BOOKING_MSTR_DIM_TABLE_NAME)
				.setKeyColumnName(BOOKING_MSTR_DIM_KEY_COLUMN)
				.setLoadMode(LoadMode.REPLACE)
				.setIgnoreLastColumn(true)
				.setReportIds(BOOKING_MSTR_DIM_REPORT_ID).build();
		
		proc.process();
	}
}
