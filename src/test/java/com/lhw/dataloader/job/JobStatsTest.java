package com.lhw.dataloader.job;

import static org.junit.Assert.*;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lhw.dataloader.job.JobStats.JobState;

public class JobStatsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testJobStats() {
		JobStats jobStats = new JobStats();
		jobStats.setName("Parent stats");
		jobStats.setStartTime(new Date());
		
		for (int i = 0; i < 10; i++) {
			JobStats childJobStats = new JobStats();
			childJobStats.setName("Child stats " + i);
			childJobStats.setStartTime(new Date());
			childJobStats.addMessage("Child stats message " + i);
			childJobStats.setEndTime(new Date());
			childJobStats.setState(JobState.SUCCEEDED);
			jobStats.addChildJobStats(childJobStats);
		}
		
		jobStats.setEndTime(new Date());
		jobStats.setState(JobState.SUCCEEDED);
		
		System.out.println(jobStats.buildEmailBody());
		assertTrue(jobStats.isSucceeded());
	}

}
