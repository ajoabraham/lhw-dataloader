package com.lhw.dataloader.job.reader;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lhw.dataloader.common.AppProps;
import com.lhw.dataloader.common.CommonConstants;
import com.lhw.dataloader.model.DataRow;
import com.lhw.dataloader.model.HeaderRow;
import com.lhw.dataloader.util.CommonUtils;

public class RestDataReaderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDimDataReader() throws ReaderException {
		String dataUrl = AppProps.I.getDimDataUrlTemplate()
				.replace(CommonConstants.PARAM_REPORT_ID, "625B571543BD51488691A889CACDA2E2")
				.replace(CommonConstants.PARAM_DELIMITER, CommonUtils.encodeUrl(AppProps.I.getCsvDelimiter()));
		try (RestDataReader reader = new RestDataReader(AppProps.I.getSessionStateUrl(), dataUrl)) {
    		HeaderRow headerRow = reader.getHeaderRow();
    	    assertTrue(headerRow.size() > 0);
    	    
    	    // Read data
    	    DataRow dataRow = null;
    	    
    	    int count = 0;
    	    while ((dataRow = reader.readDataRow()) != null) {
    	    	assertEquals(headerRow.size(), dataRow.size());
    	    	count++;
    	    }
    	    
    	    System.out.println("Read total " + count + " rows");
		}
	}
	
	@Test
	public void testDimDataReaderWithMultipleReportIds() throws ReaderException {
		String[] reportIds = new String[] {"3968586146085C999B46559D71845AD4",
				"7DD4F08042A7861C62A354968BD739CE", "7827FC6144CBF278629E1F8BCA5C8450"};
		
		for (String reportId : reportIds) {
			System.out.println("Reading report ID " + reportId);
			String dataUrl = AppProps.I.getDimDataUrlTemplate().replace(CommonConstants.PARAM_REPORT_ID, reportId)
					.replace(CommonConstants.PARAM_DELIMITER, CommonUtils.encodeUrl(AppProps.I.getCsvDelimiter()));
    		try (RestDataReader reader = new RestDataReader(AppProps.I.getSessionStateUrl(), dataUrl)) {
        		HeaderRow headerRow = reader.getHeaderRow();
        	    assertTrue(headerRow.size() > 0);
        	    
        	    // Read data
        	    DataRow dataRow = null;
        	    
        	    int count = 0;
        	    while ((dataRow = reader.readDataRow()) != null) {
        	    	assertEquals(headerRow.size(), dataRow.size());
        	    	count++;
        	    }
        	    
        	    System.out.println("Read total " + count + " rows");
    		}
		}
	}
	
	@Test
	public void testFactDataReader() throws ReaderException {
		String dataUrl = AppProps.I.getFactDataUrlTemplate()
				.replace(CommonConstants.PARAM_INTERVAL, CommonUtils.encodeUrl("01/25/2016^01/27/2016"))
				.replace(CommonConstants.PARAM_REPORT_ID, "1401E6CC49C4FD5CE81211963524A46C")
				.replace(CommonConstants.PARAM_DELIMITER, CommonUtils.encodeUrl(AppProps.I.getCsvDelimiter()));
		try (RestDataReader reader = new RestDataReader(AppProps.I.getSessionStateUrl(), dataUrl)) {
    		HeaderRow headerRow = reader.getHeaderRow();
    		System.out.println("Header row size: " + headerRow.size());
    	    assertTrue(headerRow.size() > 0);
    	    
    	    // Read data
    	    DataRow dataRow = null;
    	    
    	    int count = 0;
    	    while ((dataRow = reader.readDataRow()) != null) {
    	    	assertEquals(headerRow.size(), dataRow.size());
    	    	count++;
    	    }
    	    
    	    System.out.println("Read total " + count + " rows");
		}
	}
}
