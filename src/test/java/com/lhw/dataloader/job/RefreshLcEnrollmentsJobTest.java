package com.lhw.dataloader.job;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_DATE_COLUMN_1;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_DATE_COLUMN_2;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_KEY_COLUMN;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_REFRESH_REPORT_ID;
import static com.lhw.dataloader.common.JobConstants.LC_ENROLLMENTS_TABLE_NAME;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class RefreshLcEnrollmentsJobTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRefreshLcEnrollmentsDataJob() throws Exception {
		// TH 02/01/2016, this test case will load all data from last load minus refresh interval until yesterday.
//		RefreshLcEnrollmentsJob job = new RefreshLcEnrollmentsJob("lc_enrollments_base", "account_number", 
//				"lc_join_date_original", "lc_join_date_latest", "915E85BC4CE3AA50663F1AB7EC603C29");
		RefreshLcEnrollmentsJob job = new RefreshLcEnrollmentsJob(LC_ENROLLMENTS_TABLE_NAME, 
				LC_ENROLLMENTS_KEY_COLUMN, LC_ENROLLMENTS_DATE_COLUMN_1, LC_ENROLLMENTS_DATE_COLUMN_2, 
				LC_ENROLLMENTS_REFRESH_REPORT_ID);
		long startTime = System.currentTimeMillis();
		job.call();
		System.out.println("It took " + (System.currentTimeMillis() - startTime) + "ms to refresh data.");
	}
}
