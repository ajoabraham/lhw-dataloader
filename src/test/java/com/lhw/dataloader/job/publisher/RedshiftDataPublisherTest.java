package com.lhw.dataloader.job.publisher;

import java.sql.Connection;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;

import com.lhw.dataloader.util.ConnUtils;

public class RedshiftDataPublisherTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		try (Connection conn = ConnUtils.I.getConnection();
				Statement statement = conn.createStatement()) {
			statement.execute("delete from vero_schema_info where table_name = 'schema_info_test'");
		}
	}

//	@Test
//	public void testUpdateVeroSchemaInfo() throws PublisherException, SQLException {
//		RedshiftDataPublisher publisher = new RedshiftDataPublisher(null, null, null, null, LoadMode.REPLACE);
//		DataTypeInfo idTypeInfo = new DataTypeInfo("id", 0);
//		idTypeInfo.setDataType(DataType.INTEGER);
//		idTypeInfo.setLength(10);
//		DataTypeInfo nameTypeInfo = new DataTypeInfo("name", 1);
//		nameTypeInfo.setDataType(DataType.VARCHAR);
//		nameTypeInfo.setLength(200);
//		
//		List<DataTypeInfo> dataTypeInfos = Arrays.asList(idTypeInfo, nameTypeInfo);
//		
//		publisher.updateVeroSchemaInfoTable("schema_info_test", "create table schema_info_test (id int, name varchar(200))", dataTypeInfos);
//		
//		try (Connection conn = ConnUtils.I.getConnection();
//				Statement statement = conn.createStatement()) {
//			ResultSet resultSet = statement.executeQuery("select count(*) from vero_schema_info where table_name = 'schema_info_test'");
//			
//			if (resultSet.next()) {
//				assertEquals(1, resultSet.getInt(1));
//			}
//			else {
//				assertTrue("No data found in the database from vero_schema_info table", false);
//			}
//		}
//	}
}
