package com.lhw.dataloader.util;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;

import com.lhw.dataloader.common.CommonConstants;

public class DateTest {	
	@Test
	public void testDateBetween() throws ParseException {
		Date start = DateUtils.parseDate("2009-12-01", CommonConstants.DEFAULT_DATE_FORMAT);
		Date end = DateUtils.parseDate("2009-12-02", CommonConstants.DEFAULT_DATE_FORMAT);
		
		long days = CommonUtils.daysInBetween(start, end);
		assertEquals(2, days);
		
		end = DateUtils.parseDate("2009-12-31", CommonConstants.DEFAULT_DATE_FORMAT);
		days = CommonUtils.daysInBetween(start, end);
		assertEquals(31, days);
	}
	
	@Test
	public void testDateIncremental() throws ParseException {
		Date start = DateUtils.parseDate("2009-12-01", CommonConstants.DEFAULT_DATE_FORMAT);
		Date tenDaysLater = DateUtils.addDays(start, 10);
		SimpleDateFormat dateFormat = new SimpleDateFormat(CommonConstants.DEFAULT_DATE_FORMAT);
		System.out.println(dateFormat.format(tenDaysLater));
		
		Date end = DateUtils.parseDate("2011-12-31", CommonConstants.DEFAULT_DATE_FORMAT);
		
		int interval = 30;
		
		while (!DateUtils.isSameDay(start, end) && !start.after(end)) {
			long diff = CommonUtils.daysInBetween(start, end);
			
			if (diff > interval) {
				diff = interval;
			}
			
			Date intervalEnd = DateUtils.addDays(start, (int) (diff - 1));
			
			System.out.println(dateFormat.format(start) + " - " + dateFormat.format(intervalEnd));
			start = DateUtils.addDays(intervalEnd, 1);
		}
	}
	
	@Test
	public void testDateRange() throws ParseException {
		Date d = DateUtils.parseDate("2016-02-01", CommonConstants.DEFAULT_DATE_FORMAT);
		Date endDate = DateUtils.addDays(d, -1);
		System.out.println("End date " + endDate);
		assertEquals(2, CommonUtils.daysInBetween(endDate, d));
		
		int interval = 3;
		Date startDate = DateUtils.addDays(endDate, 1 - interval);
		System.out.println("Start date " + startDate);
		assertEquals(interval, CommonUtils.daysInBetween(startDate, endDate));
	}
}
