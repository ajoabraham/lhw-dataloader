package com.lhw.dataloader.util;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lhw.dataloader.model.HeaderRow;

public class DdlGeneratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDdlGenerator() {
		HeaderRow headerRow = new HeaderRow();
		headerRow.addHeaderName("Booking Source Code (Original)");
		headerRow.addHeaderName(";Booking Source Desc (Original)");
		headerRow.addHeaderName("int col");
		headerRow.addHeaderName("bigint col");
		headerRow.addHeaderName("boolean col");
		
		Map<String, TypeAnalyzer> typeAnalyzers = new HashMap<>();
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer("col", 0);
		typeAnalyzer.analyze("6981234.5678");
		typeAnalyzers.put(headerRow.getHeaderNames().get(0), typeAnalyzer);
		
		typeAnalyzer = new TypeAnalyzer("col", 0);
		typeAnalyzer.analyze("Hello World");
		typeAnalyzers.put(headerRow.getHeaderNames().get(1), typeAnalyzer);
		
		typeAnalyzer = new TypeAnalyzer("col", 0);		
		typeAnalyzer.analyze("3567");
		typeAnalyzers.put(headerRow.getHeaderNames().get(2), typeAnalyzer);
		
		typeAnalyzer = new TypeAnalyzer("col", 0);
		typeAnalyzer.analyze("129223372036854775807");
		typeAnalyzers.put(headerRow.getHeaderNames().get(3), typeAnalyzer);
		
		typeAnalyzer = new TypeAnalyzer("col", 0);
		typeAnalyzer.analyze("True");
		typeAnalyzers.put(headerRow.getHeaderNames().get(4), typeAnalyzer);
		
		String ddl = DdlGenerator.generateDdl("booking_channel_dim", CommonUtils.toDataTypeInfos(headerRow, typeAnalyzers));
		System.out.println(ddl);
	}

}
