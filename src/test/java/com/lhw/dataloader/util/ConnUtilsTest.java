package com.lhw.dataloader.util;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ConnUtilsTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetConnection() throws Exception {
		try (Connection conn = ConnUtils.I.getConnection()) {
			DatabaseMetaData metaData = conn.getMetaData();
			
			String productName = metaData.getDatabaseProductName();
			String versionInfo = metaData.getDatabaseProductVersion();
			System.out.println("Connect to " + productName + " (" + versionInfo + ")" + System.lineSeparator()
					+ " JDBC Driver: " + metaData.getDriverVersion() + System.lineSeparator()
					+ " Max Columns in Table: " + metaData.getMaxColumnsInTable());
			System.out.println("Data type information:");
			System.out.println("=======================================");
			try (ResultSet typeInfo = metaData.getTypeInfo()) {
				while (typeInfo.next()) {
					System.out.println(typeInfo.getString("TYPE_NAME")
							+ "(" + typeInfo.getString("DATA_TYPE") + ")");
				}
			}
			
			assertNotNull(productName);
			assertNotNull(versionInfo);
		}
		catch (SQLException e) {
			throw e;
		}
	}
	
	@Test
	public void testDoesTableExist() {
		assertTrue(CommonUtils.doesTableExist("lc_enrollments"));
		assertFalse(CommonUtils.doesTableExist("fake_table"));
	}

}
