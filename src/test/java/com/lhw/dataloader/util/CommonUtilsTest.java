package com.lhw.dataloader.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CommonUtilsTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsDate() {
		assertTrue(CommonUtils.isDate("3/1/1999"));
		assertTrue(CommonUtils.isDate("1999-03-01"));
		assertFalse(CommonUtils.isDate("03/01/1999 18:25:16"));
	}
	
	@Test
	public void testIsTimestamp() {
		assertFalse(CommonUtils.isTimestamp("3/1/1999"));
		assertFalse(CommonUtils.isTimestamp("1999-03-01"));
		assertTrue(CommonUtils.isTimestamp("03/01/1999 18:25:16"));
	}
	
	@Test
	public void testNormalizeColumnName() {
		assertEquals("col_1", CommonUtils.normalizeColumnName("(col____1)"));
		assertEquals("test_this_col_1", CommonUtils.normalizeColumnName("__test_This(_col____1)"));
	}
	
	@Test
	public void testConvertToDefaultDateFormat() {
		String date = CommonUtils.convertToDefaultDateFormat("04/01/2016");
		System.out.println("Date is " + date);
	}
	
	@Test
	public void testIsNumeric() {
		System.out.println("0 is numeric " + CommonUtils.isNumeric("0"));
	}
}
