package com.lhw.dataloader.util;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.lhw.dataloader.common.DataType;
import com.lhw.dataloader.model.DataTypeInfo;

public class TypeAnalyzerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInteger() {
		String[] inputs = {"0", "123456", "234", "85298", "10"};
		DataTypeInfo typeInfo = analyzeInputs(inputs);
		
		assertEquals(DataType.INTEGER, typeInfo.getDataType());
		assertEquals(6, typeInfo.getLength());
	}
	
	@Test
	public void testBigInt() {
		String[] inputs = {"0", "123456", "234", "2147483648", "10"};
		DataTypeInfo typeInfo = analyzeInputs(inputs);
		
		assertEquals(DataType.BIGINT, typeInfo.getDataType());
		assertEquals(10, typeInfo.getLength());
		
		String[] inputs2 = {"0", "123456", "234", "129223372036854775807", "10"};
		typeInfo = analyzeInputs(inputs2);
		
		assertEquals(DataType.BIGINT, typeInfo.getDataType());
		assertEquals(21, typeInfo.getLength());
	}

	@Test
	public void testDecimal() {
		String[] inputs = {"0.52", "123456.3456", "234.565385", "85298", "10.1"};
		DataTypeInfo typeInfo = analyzeInputs(inputs);
		
		assertEquals(DataType.DECIMAL, typeInfo.getDataType());
		assertEquals(11, typeInfo.getLength());
		assertEquals(12, typeInfo.getPrecision());
		assertEquals(6, typeInfo.getScale());
	}
	
	@Test
	public void testTimestamp() {
		String[] inputs = {"3/14/2014 7:35:23 AM", "6/12/2014 12:00:00 AM", "3/13/2011 2:02:27 AM", "3/14/2011 2:02:27 AM"};
		DataTypeInfo typeInfo = analyzeInputs(inputs);
		
		assertEquals(DataType.TIMESTAMP, typeInfo.getDataType());
	}
	
	private DataTypeInfo analyzeInputs(String[] inputs) {
		TypeAnalyzer typeAnalyzer = new TypeAnalyzer("col", 0);
		
		for (String input : inputs) {
			typeAnalyzer.analyze(input);
		}
		
		typeAnalyzer.printStats();
		
		return typeAnalyzer.getDataTypeInfo();
	}
}
